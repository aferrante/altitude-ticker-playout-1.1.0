unit MasterControlInterfaceModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ScktComp, Globals,
  ExtCtrls;

type
  TMasterControlInterface = class(TForm)
    MasterControlPort: TServerSocket;
    MCStatusUpdateTimer: TTimer;
    procedure MasterControlPortAccept(Sender: TObject; Socket: TCustomWinSocket);
    procedure MasterControlPortClientConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure MasterControlPortClientDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure MasterControlPortClientError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure MasterControlPortClientRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure SendCurrentStatus;
    procedure MCStatusUpdateTimerTimer(Sender: TObject);
  private

  public

  end;

var
  MasterControlInterface: TMasterControlInterface;
  MasterControlConnected: Boolean;

const
  SOH = #1; //Start of heading
  ETX = #3; //End of text
  EOT = #4; //End of transmission

implementation

{$R *.dfm}

uses Main,
     EngineIntf,
     DataModule;

//LED handlers
//Global Control connection
procedure TMasterControlInterface.MasterControlPortAccept(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.MasterControlLED.Lit := TRUE;
  //Enable timer to send status info back to MC application
  MCStatusUpdateTimer.Enabled := TRUE;
end;

procedure TMasterControlInterface.MasterControlPortClientConnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.MasterControlLED.Lit := TRUE;
end;

procedure TMasterControlInterface.MasterControlPortClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.MasterControlLED.Lit := FALSE;
  //Disable timer to send status info back to MC application
  MCStatusUpdateTimer.Enabled := FALSE;
end;

procedure TMasterControlInterface.MasterControlPortClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.MasterControlLED.Lit := FALSE;
  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

////////////////////////////////////////////////////////////////////////////////
// MASTER CONTROL COMMAND PROCESSING
////////////////////////////////////////////////////////////////////////////////
//Handler for command received from Local control port
procedure TMasterControlInterface.MasterControlPortClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
  ErrorCode: SmallInt;
  Tokens: Array[1..3] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal: SmallInt;
  TickerType: SmallInt;
  UseLocalPlaylist: Boolean;
  FoundPlaylist: Boolean;
  TickerRecPtr: ^TickerRec;
  LogStr: String;
begin
  //Get the data
  Data := Socket.ReceiveText;

  //Only act if automation interface enabled
  //Check command
  ErrorCode := 0;

  //Parse the string for the command and any parameters
  //First, check for valid command framing; should contain at least one SOH, one ETX and one EOT;
  //If not, send back error code 1 = Invalid command syntax received

  //Need to at least have framing characters; also check for SOH at start and EOT at end
  if (Length(Data) > 3) AND (Data[1] = SOH) AND (Data[Length(Data)] = EOT) then
  begin
    //Strip off SOT and EOT
    Data := Copy (Data, 2, Length(Data)-2);
    //Parse for tokens
    for i := 1 to 3 do Tokens[i] := '';
    Cursor := 1;
    TokenIndex :=1;
    repeat
      if (Data[Cursor] = ETX) then
      begin
        Inc(Cursor);
        Inc(TokenIndex);
      end;
      Tokens[TokenIndex] := Tokens[TokenIndex] + Data[Cursor];
      Inc(Cursor);
    until (Cursor > Length(Data)) OR (TokenIndex = 3);
  end
  //Error code 1 - Invalid command syntax received
  else ErrorCode := MC_INVALID_COMMAND_SYNTAX;

  //Call functions based on command
  if (Tokens[1] = 'SET_LOCAL_MODE') then
  begin
    //Call function to trigger ticker; pass in -1 if invalid integer
    ParamVal := StrToIntDef(Tokens[2], -1);

    //Disable manual mode => Go to schedule monitoring mode
    if (ParamVal = 0) then
    begin
      //Set schedule monitoring to
      MainForm.ScheduleMonitoringEnabledCheckBox.Checked := TRUE;
      ScheduleMonitoringEnabled := TRUE;
      MainForm.BitBtn4.Visible := FALSE;
      MainForm.Label6.Visible := FALSE;
      EngineInterface.WriteToAsRunLog('Operator switched to scheduled/automatic playlist load mode');
      MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application : Go to scheduled playlist load mode.';
    end
    //Go to manual mode
    else if (ParamVal = 1) then
    begin
      MainForm.ScheduleMonitoringEnabledCheckBox.Checked := FALSE;
        //Clear flag; disble controls; show label
      ScheduleMonitoringEnabled := FALSE;
      MainForm.BitBtn4.Visible := TRUE;
      MainForm.Label6.Visible := TRUE;
      EngineInterface.WriteToAsRunLog('Operator switched to manual playlist load mode');
      MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application: Go to manual playlist load mode.';
    end
    else ErrorCode := MC_INVALID_PLAYLIST_CONTROL_MODE;
  end

  //Load ticker playlist
  else if (Tokens[1] = 'LOAD') and not (RunningTicker) and not (ScheduleMonitoringEnabled) then
  begin
    FoundPlaylist := FALSE;
    with dmMain.PlaylistQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM Ticker_Groups WHERE Playlist_ID = ' +  Tokens[2]);
      Open;
      if (RecordCount > 0) then FoundPlaylist := TRUE;
    end;
    //Check database for existence of playlist
    if (FoundPlaylist) then
    begin
      //If playlist exists, load it
      MainForm.LoadPlaylistCollection(PLAYLIST_TYPE_TICKER, PLAYLIST_REPLACE, StrToFloatDef(Trim(Tokens[2]), -1));
      //Reset ticker to top
      MainForm.ResetTickerToTop;
      //Refresh the zipper grid and status labels
      MainForm.RefreshPlaylistGrid(1);
      //Set the current playlist ID - allows for refresh of collection of more than one iteration
      CurrentTickerPlaylistID := StrToFloatDef(Trim(Tokens[2]), -1);
      //Added to ensure check schedule thread picks up scheduled playlist when exiting manual load mode
      CurrentTickerPlaylistStartTime := 0;
      //Update labels
      ScheduledPlaylistInfo[1].Playlist_Description := dmMain.PlaylistQuery.FieldByName('Playlist_Description').AsString;
      MainForm.TickerPlaylistName.Caption := ScheduledPlaylistInfo[1].Playlist_Description;
      MainForm.TickerStartTimeLabel.Caption := 'N/A';
      MainForm.TickerEndTimeLabel.Caption := 'N/A';
      //Added logging V3.0.14
      EngineInterface.WriteToAsRunLog('Ticker playlist loaded remotely: ' + ScheduledPlaylistInfo[1].Playlist_Description +
        ' (Playlist ID ' + FloatToStr(CurrentTickerPlaylistID) + ')');
      //Go to bottom of playlist if ticker is already running so we loop back to the top; allow only for 1-line mode
      if (RunningTicker) then MainForm.ResetTickerToBottom;
      //Set the ticker mode and refresh the controls
      TickerDisplayMode := dmMain.PlaylistQuery.FieldByName('Ticker_Mode').AsInteger;
      EngineInterface.WriteToAsRunLog('Ticker display mode for Playlist ID ' + FloatToStr(CurrentTickerPlaylistID) + ': ' + IntToStr(TickerDisplayMode));
      //Added logging V3.0.14
      //Log first entry in playlist
      if (Ticker_Collection.Count > 0) then
      begin
        TickerRecPtr := Ticker_Collection.At(0);
        LogStr := TickerRecPtr^.League + ' ';
        if (TickerRecPtr^.Record_Type = 1) OR (TickerRecPtr^.Record_Type = 37) OR (TickerRecPtr^.Record_Type = 45) then
          LogStr := LogStr + TickerRecPtr^.SponsorLogo_Name
        else if (TickerRecPtr^.Record_Type = 2) OR (TickerRecPtr^.Record_Type = 3) then
          LogStr := LogStr + TickerRecPtr^.UserData[1]
        else if (MainForm.GetTemplateInformation(TickerRecPtr^.Template_ID).UsesGameData = TRUE) then
          LogStr := LogStr + TickerRecPtr^.Description
        else
          LogStr := LogStr + TickerRecPtr^.UserData[1];
        //Set commends
        LogStr := LogStr + ' ' + TickerRecPtr^.Comments; //Record enable
        EngineInterface.WriteToAsRunLog('First Ticker Record Data for Playlist ID ' + FloatToStr(CurrentTickerPlaylistID) + ': ' + LogStr);
      end;
      Case TickerDisplayMode of
           //1-line, 1-time
        1: begin
             MainForm.OneTimeLoopModeLabel.Visible := TRUE;
             MainForm.TickerMode.Caption := '1-LINE MODE';
           end;
           //1-line, loop
        2: begin
             MainForm.OneTimeLoopModeLabel.Visible := FALSE;
             MainForm.TickerMode.Caption := '1-LINE MODE';
           end;
           //2-line, 1-time
        3: begin
             MainForm.OneTimeLoopModeLabel.Visible := TRUE;
             MainForm.TickerMode.Caption := '2-LINE MODE';
           end;
           //2-line, loop
        4: begin
             MainForm.OneTimeLoopModeLabel.Visible := FALSE;
             MainForm.TickerMode.Caption := '2-LINE MODE';
           end;
      end;
      MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application: Load playlist: ' + ScheduledPlaylistInfo[1].Playlist_Description;
    end
    //Set error code for invalid TICKER playlist name
    else ErrorCode := MC_INVALID_TICKER_PLAYLIST_ID;

    //Close the query
   dmMain.PlaylistQuery.Close;
  end

  //Start ticker
  else if (Tokens[1] = 'START') then
  begin
    //Call function to start ticker
    MainForm.TriggerCurrentTicker;
    MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application: Start Ticker';
  end

  //Stop ticker
  else if (Tokens[1] = 'STOP') then
  begin
    //Call function to abort ticker
    if (RunningTicker) then MainForm.AbortCurrentEvent;
    MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application: Stop Ticker';
  end

  //Reset ticker to top
  else if (Tokens[1] = 'RESET_TICKER') then
  begin
    MainForm.ResetTickerToTop;
    MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application: Reset Ticker to Top';
  end

  //Reset error
  else if (Tokens[1] = 'RESET_ERROR') then
  begin
    //Reset error flag and status panel color
    Error_Condition := False;

    //Reset captions for labels on status panel
    MainForm.Label5.Caption := 'OK';
    MainForm.Panel3.Color := clBtnFace;
    MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Received command from Master Control application: Reset Error';

    MainForm.StatusBar.Color := clBtnFace;
    MainForm.StatusBar.SimpleText := ' ';
  end

  //Unidentified error - command could not be processed
  else ErrorCode := MC_UNIDENTIFIED_ERROR;
end;

//Pocedure to send the automation status to the Master Control application
procedure TMasterControlInterface.SendCurrentStatus;
var
  i: SmallInt;
  Param: Array[1..6] of String;
  CmdStr: String;
begin
  //Init to blanks
  for i := 1 to 6 do Param[i] := ' ';

  //Display the status
  //Field 1 = Error code for current error
  if (Error_Condition) then
    Param[1] := '1'
  else
    Param[1] := '0';

  //Field 2 = Ticker graphics engine status
  if (EngineInterface.EnginePort.Active) then
    Param[2] := '1'
  else
    Param[2] := '0';

  //Field 3 = Ticker running/halted status
  if (RunningTicker) then
    Param[3] := '1'
  else
    Param[3] := '0';

  //Field 4 = Current Mode (Scheduled/Manual)
  if (ScheduleMonitoringEnabled) then
    Param[4] := '0'
  else
    Param[4] := '1';

  //Field 5 = Current ticker playlist name
  if (Trim(ScheduledPlaylistInfo[1].Playlist_Description) = '') then
    Param[5] := 'N/A'
  else
    Param[5] := ScheduledPlaylistInfo[1].Playlist_Description;

  //Field 6 = Spare
  Param[6] := ' ';

  if (MasterControlPort.Active) AND (MasterControlPort.Socket.ActiveConnections > 0) then
  begin
    CmdStr := SOH + 'STATUS' + ETX +  Param[1] + ETX +
      Param[2] + ETX + Param[3] + ETX +  Param[4] + ETX + Param[5] + ETX + Param[6] + EOT;
    MasterControlPort.Socket.Connections[0].SendText(CmdStr);
  end;
end;

//Handler for timer to send status back to Master Control application
procedure TMasterControlInterface.MCStatusUpdateTimerTimer(Sender: TObject);
begin
  if (MasterControlPort.Active) AND (MasterControlPort.Socket.ActiveConnections > 0) then
    SendCurrentStatus;
end;

end.
