// This unit contains functions related to getting game data from Sportsticker and Stats Inc.
// Original Rev: 12/21/2009
unit GameDataFunctions;

interface

uses  SysUtils,
      Globals,
      Main, //Main form
      DataModule, //Data module
      EngineIntf;

  function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
  function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
  function GetGameData (League: String; GCode: String): GameRec;
  function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
  function GetLastName(FullName: String): String;
  function GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String; CurrentEntryIndex: SmallInt;
                            CurrentGameData: GameRec; TickerMode: SmallInt): String;

  //Function to pull weather forecast data for the home stadium
  function GetWeatherForecastData(League: String; HomeStatsID: LongInt): WeatherForecastRec;

implementation

////////////////////////////////////////////////////////////////////////////////
//Function to parse the player last name out of a full name string
////////////////////////////////////////////////////////////////////////////////
function GetLastName(FullName: String): String;
var
  i: SmallInt;
  OutStr: String;
  Cursor: SmallInt;
begin
  OutStr := FullName;
  Cursor := Pos(',', FullName);
  if (Cursor > 1) then
  begin
    OutStr := '';
    for i := 1 to Cursor-1 do
      OutStr := OutStr + FullName[i];
  end;
  GetLastName := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to format the game clock into minutes and seconds
////////////////////////////////////////////////////////////////////////////////
function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
var
  SecsStr: String;
  OutStr: String;
begin
  //Check for manual game as indicated by min, sec = -1
  if (TimeSec = -1) AND (TimeMin = -1) then OutStr := ''
  else begin
    if (TimeSec < 10) then SecsStr := '0' + IntToStr(TimeSec)
    else SecsStr := IntToStr(TimeSec);
    if (TimeMin > 0) then
      OutStr := IntToStr(TimeMin) + ':' + SecsStr
    else
      OutStr := ':' + SecsStr;
  end;
  FormatGameClock := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to take a league and a game phase code, and return a game phase record
////////////////////////////////////////////////////////////////////////////////
function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.End_Is_Half := FALSE;
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := Trim(League);
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := Trim(GamePhaseRecPtr^.Label_Period);
        OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Half := Trim(GamePhaseRecPtr^.Display_Half);
        OutRec.Display_Final := Trim(GamePhaseRecPtr^.Display_Final);
        OutRec.Display_Extended1 := Trim(GamePhaseRecPtr^.Display_Extended1);
        OutRec.Display_Extended2 := Trim(GamePhaseRecPtr^.Display_Extended2);
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to run stored procedures to get game information
////////////////////////////////////////////////////////////////////////////////
function GetGameData (League: String; GCode: String): GameRec;
var
  OutRec: GameRec;
begin
  //Check if valid league first
  if (League <> 'NONE') then
  begin
    try
      //Execute required stored procedure
      dmMain.GameDataQuery.Active := FALSE;
      dmMain.GameDataQuery.SQL.Clear;
      dmMain.GameDataQuery.SQL.Add('/* */sp_GetSI' + League + 'GameInfoCSN' + ' ' + QuotedStr(GCode));
      dmMain.GameDataQuery.Open;
      //Check for record count > 0 and process
      if (dmMain.GameDataQuery.RecordCount > 0) then
      begin
        OutRec.League := dmMain.GameDataQuery.FieldByName('League').AsString;
        //Get extra fields if it's an MLB game
        if (League = 'MLB') then
        begin
          OutRec.SubLeague := dmMain.GameDataQuery.FieldByName('SubLeague').AsString;
          OutRec.DoubleHeader := dmMain.GameDataQuery.FieldByName('DoubleHeader').AsBoolean;
          OutRec.DoubleHeaderGameNumber := dmMain.GameDataQuery.FieldByName('DoubleHeaderGameNumber').AsInteger;
          OutRec.TimeMin := 0;
          OutRec.TimeSec := 0;
          OutRec.Count := dmMain.GameDataQuery.FieldByName('Count').AsString;;
          OutRec.Situation := dmMain.GameDataQuery.FieldByName('Situation').AsString;
          OutRec.Batter := dmMain.GameDataQuery.FieldByName('Batter').AsString;
          OutRec.Baserunners[1] := dmMain.GameDataQuery.FieldByName('FirstBaserunner').AsString;
          OutRec.Baserunners[2] := dmMain.GameDataQuery.FieldByName('SecondBaserunner').AsString;
          OutRec.Baserunners[3] := dmMain.GameDataQuery.FieldByName('ThirdBaserunner').AsString;
        end
        else if (League = 'CFB') OR (League = 'CFL') OR (League = 'NFL')then
        begin
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.TimeMin := dmMain.GameDataQuery.FieldByName('TimeMin').AsInteger;
          OutRec.TimeSec := dmMain.GameDataQuery.FieldByName('TimeSec').AsInteger;
          OutRec.Count := '000';
          OutRec.Situation := dmMain.GameDataQuery.FieldByName('Situation').AsString;;
          OutRec.Batter := '';
          OutRec.Baserunners[1] := '';
          OutRec.Baserunners[2] := '';
          OutRec.Baserunners[3] := '';
        end
        else begin
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.TimeMin := dmMain.GameDataQuery.FieldByName('TimeMin').AsInteger;
          OutRec.TimeSec := dmMain.GameDataQuery.FieldByName('TimeSec').AsInteger;
          OutRec.Count := '000';
          OutRec.Situation := 'FFF';
          OutRec.Batter := '';
          OutRec.Baserunners[1] := '';
          OutRec.Baserunners[2] := '';
          OutRec.Baserunners[3] := '';
        end;
        //Set team strength variables for NHL
        if (League = 'NHL') then
        begin
          if (ANSIUpperCase(dmMain.GameDataQuery.FieldByName('VStrength').AsString) = 'Powerplay') then
            OutRec.Visitor_Param1 := 1
          else
            OutRec.Visitor_Param1 := 0;
          if (ANSIUpperCase(dmMain.GameDataQuery.FieldByName('HStrength').AsString) = 'Powerplay') then
            OutRec.Home_Param1 := 1
          else
            OutRec.Home_Param1 := 0;
          OutRec.Visitor_Param2 := 0;
          OutRec.Home_Param2 := 0;
        end
        //Set hits & errors for MLB linescore
        else if (League = 'MLB') then
        begin
          OutRec.Visitor_Param1 := StrToIntDef(dmMain.GameDataQuery.FieldByName('VHits').AsString, 0);
          OutRec.Visitor_Param2 := StrToIntDef(dmMain.GameDataQuery.FieldByName('VErrors').AsString, 0);
          OutRec.Home_Param1 := StrToIntDef(dmMain.GameDataQuery.FieldByName('HHits').AsString, 0);
          OutRec.Home_Param2 := StrToIntDef(dmMain.GameDataQuery.FieldByName('HErrors').AsString, 0);
        end
        //Set to defaults for all other sports
        else begin
          OutRec.Visitor_Param1 := 0;
          OutRec.Visitor_Param2 := 0;
          OutRec.Home_Param1 := 0;
          OutRec.Home_Param2 := 0;
        end;
        //If college, get ranks
        if (League = 'NCAAB') OR (League = 'NCAAF') OR (League = 'NCAAW') OR
           (League = 'CFB') OR (League = 'CBK') OR (League = 'WCBK') then
        begin
          OutRec.VRank := dmMain.GameDataQuery.FieldByName('VRank').AsString;
          if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
          OutRec.HRank := dmMain.GameDataQuery.FieldByName('HRank').AsString;
          if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
        end
        else begin
          OutRec.VRank := ' ';
          OutRec.HRank := ' ';
        end;
        //Set remaining parameters
        OutRec.HStatsIncID := dmMain.GameDataQuery.FieldByName('HId').AsInteger;
        OutRec.VStatsIncID := dmMain.GameDataQuery.FieldByName('VId').AsInteger;
        OutRec.HName := dmMain.GameDataQuery.FieldByName('HName').AsString;
        OutRec.VName := dmMain.GameDataQuery.FieldByName('VName').AsString;
        OutRec.HMnemonic := dmMain.GameDataQuery.FieldByName('HMnemonic').AsString;
        OutRec.VMnemonic := dmMain.GameDataQuery.FieldByName('VMnemonic').AsString;
        OutRec.HScore := dmMain.GameDataQuery.FieldByName('HScore').AsInteger;
        OutRec.VScore := dmMain.GameDataQuery.FieldByName('VScore').AsInteger;
        OutRec.Phase := dmMain.GameDataQuery.FieldByName('Phase').AsInteger;
        //Set the game state
        if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Pre-Game') then
          OutRec.State := 1
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'In-Progress') then
          OutRec.State := 2
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Final') then
          OutRec.State := 3
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Postponed') then
          OutRec.State := 4
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Delayed') then
          OutRec.State := 5
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Suspended') then
          OutRec.State := 6
        else
          OutRec.State := -1;
        //Set state for in-progress check
        OutRec.StateForInProgressCheck := OutRec.State;
        OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName;
        OutRec.Date := dmMain.GameDataQuery.FieldByName('Date').AsDateTime;
        OutRec.StartTime := dmMain.GameDataQuery.FieldByName('StartTime').AsDateTime;
        OutRec.DataFound := TRUE;
      end
      else OutRec.DataFound := FALSE;
    except
      if (ErrorLoggingEnabled = True) then
        EngineInterface.WriteToErrorLog('Error occurred in GetGameData function for full game data');
      //Clear data found flag
      OutRec.DataFound := FALSE;
    end;
  end;
  //Return
  GetGameData := OutRec;
end;

//Function to get football game situation data
function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
var
  i: SmallInt;
  OutRec: FootballSituationRec;
  VTeamID, HTeamID, PossessionTeamID: Double;
  PossessionYardsFromGoal: SmallInt;
  TeamRecPtr: ^TeamRec;
  StrCursor, SubStrCounter: SmallInt;
  Substrings: Array[1..4] of String;
begin
  //Init
  HTeamID := 0;
  VTeamID := 0;
  StrCursor := 1;
  SubStrCounter := 1;
  for i := 1 to 4 do Substrings[i] := '';

  //Parse out situation string to components
  While (StrCursor <= Length(CurrentGameData.Situation)) do
  begin
    //Get substrings for data fields
    While (CurrentGameData.Situation[StrCursor] <> '-') do
    begin
      SubStrings[SubStrCounter] := SubStrings[SubStrCounter] + CurrentGameData.Situation[StrCursor];
      Inc(StrCursor);
    end;
    //Go to next field
    Inc(StrCursor);
    Inc(SubStrCounter);
  end;

  //Set values
  //Do ID of team with possession
  if (Trim(SubStrings[1]) <> '') then
  begin
    try
      PossessionTeamID := Trunc(StrToFloat(Substrings[1]));
    except
      PossessionTeamID := 0;
      EngineInterface.WriteToErrorLog('Error occurred while trying to convert team ID for football possession');
    end;
  end
  else PossessionTeamID := 0;

  //Get yards from goal
  if (Trim(SubStrings[2]) <> '') then
  begin
    try
      PossessionYardsFromGoal := StrToInt(Substrings[2]);
    except
      PossessionYardsFromGoal := 0;
      EngineInterface.WriteToErrorLog('Error occurred while trying to convert field position for football possession');
    end;
  end
  else PossessionYardsFromGoal := 0;

  //Get team IDs of visiting & home teams
  if (Team_Collection.Count > 0) then
  begin
    for i := 0 to Team_Collection.Count-1 do
    begin
      TeamRecPtr := Team_Collection.At(i);
      if (CurrentGameData.VMnemonic = TeamRecPtr^.DisplayName1) AND
         (CurrentGameData.League = TeamRecPtr^.League) then
        VTeamID := TeamRecPtr^.StatsIncId;
    end;
    for i := 0 to Team_Collection.Count-1 do
    begin
      TeamRecPtr := Team_Collection.At(i);
      if (CurrentGameData.HMnemonic = TeamRecPtr^.DisplayName1) AND
         (CurrentGameData.League = TeamRecPtr^.League) then
        HTeamID := TeamRecPtr^.StatsIncId;
    end;
  end;
  //Check to find team with possession
  if (VTeamID = PossessionTeamID) then
  begin
    OutRec.VisitorHasPossession := TRUE;
    OutRec.HomeHasPossession := FALSE;
  end
  else if (HTeamID = PossessionTeamID) then
  begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := TRUE;
  end
  else begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := FALSE;
  end;
  OutRec.YardsFromGoal := PossessionYardsFromGoal;
  //Set return value
  GetFootballGameSituationData := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a symbolic name and return the value of the string to be
// sent to the engine
////////////////////////////////////////////////////////////////////////////////
function GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String;
                                           CurrentEntryIndex: SmallInt; CurrentGameData: GameRec;
                                           TickerMode: SmallInt): String;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  OutStr, OutStr2, OutStr3: String;
  TextFieldBias: SmallInt;
  GamePhaseData: GamePhaseRec;
  BaseName: String;
  CharPos: SmallInt;
  Suffix: String;
  SwitchPosMixed, SwitchPosUpper: SmallInt;
  SwitchPosStartBlack, SwitchPosEndBlack: SmallInt;
  TempStr: String;
  Days: array[1..7] of string;
begin
  //Init
  OutStr := SymbolName;
  BaseName := '';
  Suffix := '';
  LongTimeFormat := 'h:mm AM/PM';

  //Set text field bias
  TextFieldBias := 0;

  //Get info on current record
  if (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
     TICKER: TickerRecPtr := Ticker_Collection.At(CurrentEntryIndex);
     BREAKINGNEWS: BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentEntryIndex);
    end;
  end;

  //Extract out base symbol name and any suffix
  CharPos := Pos('|', SymbolName);
  if (CharPos > 0) then
  begin
    //Get base name
    if (CharPos > 1) then
      for i := 1 to CharPos-1 do BaseName := BaseName + SymbolName[i];
    //Get suffix
    if (Length(Symbolname) > CharPos) then
      for i := CharPos+1 to Length(Symbolname) do Suffix := Suffix + SymbolName[i];
    //Set new base name
    SymbolName := BaseName;
  end;
  //Blank
  if (SymbolName = '$Blank') then OutStr := ' '
  //Sponsor logo
  else if (SymbolName = '$Sponsor_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := TickerRecPtr^.SponsorLogo_Name;
      end;
    end
    else OutStr := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
  end
  //League
  else if (SymbolName = '$League') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      //TICKER: OutStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
      TICKER: OutStr := UpperCase(TickerRecPtr^.League);
    end;
  end

  //User-defined text fields
  else if (SymbolName = '$Text_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: //begin
               //DISABLE FOR NOW - CAUSES PROBLEMS WITH OTHER TEMPLATES
               //Check for doubleheader in baseball
               //if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.State = 3) then
               //begin
                 //End of game, so display game number in note field
               //  if (CurrentGameData.DoubleHeaderGameNumber = 1) then
               //    OutStr := 'GAME 1'
               //  else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
               //    OutStr := 'GAME 2'
               //  else OutStr := ' ';
               //end
               //else
                OutStr := TickerRecPtr^.UserData[1+TextFieldBias];
              //end;
      BREAKINGNEWS: OutStr := BreakingNewsRecPtr^.UserData[1];
    end;
  end
  else if (SymbolName = '$Text_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[2+TextFieldBias];
      BREAKINGNEWS: OutStr := BREAKINGNEWSRecPtr^.UserData[2];
    end;
  end
  else if (SymbolName = '$Text_3') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[3+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_4') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[4+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_5') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[5+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_6') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[6+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_7') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[7+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_8') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[8+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_9') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[9+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_10') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[10];
    end;
  end
  else if (SymbolName = '$Text_11') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[11];
    end;
  end
  else if (SymbolName = '$Text_12') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[12];
    end;
  end
  else if (SymbolName = '$Text_13') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[13];
    end;
  end
  else if (SymbolName = '$Text_14') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[14];
    end;
  end
  else if (SymbolName = '$Text_15') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[15+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_16') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[16+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_17') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[17+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_18') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[18+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_19') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[19+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_20') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[20+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_21') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_22') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_23') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[23+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_24') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[24+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_25') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[25+TextFieldBias];
    end;
  end

  //Includes AM/PM + timezone suffix
  else if (SymbolName = '$Start_Time') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      OutStr := Days[DayOfWeek(CurrentGameData.Date)];
    end
    else begin
      //Check for doubleheader in baseball, game #2
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      //Start time offset added for Version 1.0.6  03/31/08
      else OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;
    end;
    OutStr := Trim(OutStr);
  end
  //Remove timezone only (includes AM/PM)
  else if (SymbolName = '$Start_Time_No_Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      OutStr := Days[DayOfWeek(CurrentGameData.Date)];
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      //Start time offset added for Version 1.0.6  03/31/08
      else OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
    end;
    OutStr := Trim(OutStr);
  end
  //Remove AM/PM suffix
  else if (SymbolName = '$Start_Time_No_Suffix') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      OutStr := Days[DayOfWeek(CurrentGameData.Date)];
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      else begin
        OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;;
        OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
        OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
      end;
    end;
    OutStr := Trim(OutStr);
  end
  //No AM/PM or timezone suffixes
  else if (SymbolName = '$Start_Time_No_Suffix_No_Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      OutStr := Days[DayOfWeek(CurrentGameData.Date)];
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      else begin
        OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
        OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
        OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
      end;
    end;
    OutStr := Trim(OutStr);
  end
  //Timezone suffix only
  else if (SymbolName = '$Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      OutStr := '';
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := ''
      else OutStr := TimeZoneSuffix;
    end;
    OutStr := Trim(OutStr);
  end
  //AM/PM suffix only
  else if (SymbolName = '$AMPM_StartTime_Suffix') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
      OutStr := ''
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := ''
      else begin
        if (Pos('AM', TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset)) > 0) then OutStr := 'AM'
        else if (Pos('PM', TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset)) > 0) then OutStr := 'PM'
        else OutStr := '';
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //Team ranks - will only be applicable for NCAAB & NCAAF
  else if (SymbolName = '$Visitor_Rank') then OutStr := CurrentGameData.VRank
  else if (SymbolName = '$Home_Rank') then OutStr := CurrentGameData.HRank

  //Standard team name
  else if (SymbolName = '$Visitor_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Set to gold color if game is final
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Visiting team name with leading indicator
  else if (SymbolName = '$Visitor_Name_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Visiting team leading indicator
  else if (SymbolName = '$Visitor_Leader_Indicator') then
  begin
    //If in progress and team leading, set indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR
    else OutStr := '';
  end

  else if (SymbolName = '$Home_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0)then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Home team name with leading indicator
  else if (SymbolName = '$Home_Name_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Home team leading indicator
  else if (SymbolName = '$Home_Leader_Indicator') then
  begin
    //If in progress and team leading, set indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR
    else OutStr := '';
  end

  //Visitor mnemonic
  else if (SymbolName = '$Visitor_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Visitor mnemonic with leader indicator
  else if (SymbolName = '$Visitor_Mnemonic_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Home mnemonic
  else if (SymbolName = '$Home_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Home mnemonic with leader indicator
  else if (SymbolName = '$Home_Mnemonic_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  else if (SymbolName = '$Matchup_TeamNames') then
  begin
    OutStr := UpperCase(CurrentGameData.VName) + '  at  '+ UpperCase(CurrentGameData.HName);
  end
  else if (SymbolName = '$Matchup_TeamNames_Ranked') then
  begin
    OutStr := '[f 3][c 20]' + Trim(CurrentGameData.VRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.VName) + '  at  '+
      '[f 3][c 20]' + Trim(CurrentGameData.HRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.HName);
  end
  else if (SymbolName = '$Matchup_TeamMnemonics') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic) + '  at  '+ UpperCase(CurrentGameData.HMnemonic);
  end
  else if (SymbolName = '$Matchup_TeamMnemonics_Ranked') then
  begin
    OutStr := '[f 3][c 20]' + Trim(CurrentGameData.VRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.VMnemonic) + '  at  '+
      '[f 3][c 20]' + Trim(CurrentGameData.HRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.HMnemonic);
  end

  //Scores
  else if (SymbolName = '$Visitor_Score') then
  begin
    OutStr := IntToStr(CurrentGameData.VScore);
  end
  else if (SymbolName = '$Visitor_Score_Highlighted_Final') then
  begin
    OutStr := IntToStr(CurrentGameData.VScore);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end
  else if (SymbolName = '$Home_Score') then
  begin
    OutStr := IntToStr(CurrentGameData.HScore);
  end
  else if (SymbolName = '$Home_Score_Highlighted_Final') then
  begin
    OutStr := IntToStr(CurrentGameData.HScore);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Baseball linescore
  else if (SymbolName = '$MLB_Visitor_Hits') then
    OutStr := IntToStr(CurrentGameData.Visitor_Param1)
  else if (SymbolName = '$MLB_Visitor_Errors') then
    OutStr := IntToStr(CurrentGameData.Visitor_Param2)
  else if (SymbolName = '$MLB_Home_Hits') then
    OutStr := IntToStr(CurrentGameData.Home_Param1)
  else if (SymbolName = '$MLB_Home_Errors') then
    OutStr := IntToStr(CurrentGameData.Home_Param2)

  //Baseball baserunner situation
  else if (SymbolName = '$MLB_Baserunner_Situation') then
  begin
    if (Trim(CurrentGameData.Situation) = 'FFF') then OutStr := #240
    else if (Trim(CurrentGameData.Situation) = 'TFF') then OutStr := #241
    else if (Trim(CurrentGameData.Situation) = 'FTF') then OutStr := #242
    else if (Trim(CurrentGameData.Situation) = 'TTF') then OutStr := #243
    else if (Trim(CurrentGameData.Situation) = 'FFT') then OutStr := #244
    else if (Trim(CurrentGameData.Situation) = 'TFT') then OutStr := #245
    else if (Trim(CurrentGameData.Situation) = 'FTT') then OutStr := #246
    else if (Trim(CurrentGameData.Situation) = 'TTT') then OutStr := #247;
  end

  //MLB Team and # outs
  else if (SymbolName = '$MLB_Outs') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    if (Length(CurrentGameData.Count) >= 3) then
    begin
      if (CurrentGameData.Count[3] = '0') then TempStr := '0 outs'
      else if (CurrentGameData.Count[3] = '1') then TempStr := '1 out'
      else if (CurrentGameData.Count[3] = '2') then TempStr := '2 outs'
      else if (CurrentGameData.Count[3] = '3') then TempStr := '3 outs';
      //Visitor at bat
      if ((GamePhaseData.SI_Phase_Code MOD 2) = 0) then
        OutStr :=  '[f 2][c 19]' + UpperCase(CurrentGameData.VMnemonic) + ': ' +
        '[f 2][c 1]' + TempStr
      //Home team at bat
      else
        OutStr :=  '[f 2][c 19]' + UpperCase(CurrentGameData.HMnemonic) + ': ' +
        '[f 2][c 1]' + TempStr;
    end
  end

  //MLB batter & baserunners
  else if (SymbolName = '$MLB_Batter') then
  begin
    if (Trim(CurrentGameData.Batter) <> '') then
      OutStr := GetLastName(Trim(CurrentGameData.Batter)) + ' at bat'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Batter_Abbreviated') then
  begin
    if (Trim(CurrentGameData.Batter) <> '') then
      OutStr := 'AB: ' + GetLastName(Trim(CurrentGameData.Batter))
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner1') then
  begin
    if (Trim(CurrentGameData.Baserunners[1]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[1]) + ' on 1st'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner2') then
  begin
    if (Trim(CurrentGameData.Baserunners[2]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[2]) + ' on 2nd'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner3') then
  begin
    if (Trim(CurrentGameData.Baserunners[3]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[3]) + ' on 3rd'
    else OutStr := '';
  end

  //Game clock
  else if (SymbolName = '$Game_Clock') then
  begin
    if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
      OutStr := ''
    //Special case for baseball
    else begin
      TempStr := CurrentGameData.League;
      if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'NL') OR (TempStr = 'AL') OR (TempStr = 'GPFT') OR
         (TempStr = 'CAC') then
        OutStr := ' '
      else
        OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec)
    end;
  end

  //Game phase
  else if (SymbolName = '$Game_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'IL') OR (TempStr = 'AL') OR (TempStr = 'NL') then
          OutStr := UpperCase(GamePhaseData.Display_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period)
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end;
  end

  //Game phase with final highlighted in gold
  else if (SymbolName = '$Game_Phase_Highlighted_Final') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'IL') OR (TempStr = 'AL') OR (TempStr = 'NL') then
          OutStr := UpperCase(GamePhaseData.Display_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period)
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := UpperCase(GamePhaseData.Display_Period);
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := '[f 2][c 19]' + UpperCase(GamePhaseData.Display_Final);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end;
  end

  //Game clock + phase
  else if (SymbolName = '$Game_Clock_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then
    begin
      OutStr := TimeToStr(CurrentGameData.StartTime) + ' ' + TimeZoneSuffix;
    end
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(Trim(CurrentGameData.Subleague));
        //Note addition of check for "IL" - used by Stats Inc. for inter-league games
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'IL') OR (TempStr = 'AL') OR (TempStr = 'NL') then
          OutStr := UpperCase(GamePhaseData.Display_Period)
        else
          OutStr := 'END ' + UpperCase(GamePhaseData.Display_Period);
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
        OutStr := UpperCase(GamePhaseData.Display_Half)
      else
        OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec) + '  ' +
          UpperCase(GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase).Display_Period);
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := UpperCase(GamePhaseData.Display_Final);
      //Check for doubleheader in baseball and append suffix if applicable
      if (CurrentGameData.DoubleHeader = TRUE) then
      begin
        //End of game, so display game number in note field
        if (CurrentGameData.DoubleHeaderGameNumber = 1) then
          OutStr := OutStr + ' - GM 1'
        else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' - GM 2'
      end;
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end
    else OutStr := '';
  end

  //Game inning (baseball only)
  else if (SymbolName = '$Game_Inning') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Remove top/bottom inning indicator characters
    GamePhaseData.Display_Period := StringReplace(GamePhaseData.Display_Period, '�', '', [rfReplaceAll]);
    GamePhaseData.Display_Period := StringReplace(GamePhaseData.Display_Period, '�', '', [rfReplaceAll]);

    //In-Game
    if (CurrentGameData.State = INPROGRESS) then
      OutStr := GamePhaseData.Display_Period
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SPD';
    end
    else
      OutStr := ' ';
  end

  //Top/bottom of inning indicator
  else if (SymbolName = '$Inning_Indicator') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //In-game only; check for upper-ASCII character as first in string
    if (CurrentGameData.State = 2) AND
       (Length(GamePhaseData.Display_Period) > 0) AND
       (Ord(GamePhaseData.Display_Period[1]) > 128) then
      OutStr := GamePhaseData.Display_Period[1]
    else OutStr := '';
  end

  //Football field position
  else if (SymbolName = '$Football_Field_Position') then
  begin
    if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
    begin
      //Set tab stops
      TempStr := '[x 0 5 11 16 22 27 33 38 44 49 55 60 66 71 77 82 88 93 99 105 110]';
      //Set tab position
      //NFL or NCAAF - Field is 100 yards long
      if (CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF') then
      begin
        TempStr := TempStr + '[t ';
        //Visiting team runs from tab 0 to tab 20
        TempStr := TempStr + IntToStr(20 - (GetFootballGameSituationData(CurrentGameData).YardsFromGoal DIV 5));
        TempStr := TempStr + ']';
      end;
      //Add arrow indicator - point right for visiting team
      TempStr := TempStr + FIELDARROWRIGHT;
    end
    else if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
    begin
      //Set tab stops
      TempStr := '[x 3 8 14 19 25 30 36 41 47 52 58 63 69 74 81 85 91 96 102 108 131]';
      //Set tab position
      //NFL or NCAAF - Field is 100 yards long
      if (CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF') then
      begin
        TempStr := TempStr + '[t ';
        //Visiting team runs from tab 0 to tab 20
        TempStr := TempStr + IntToStr(3 + (GetFootballGameSituationData(CurrentGameData).YardsFromGoal DIV 5));
        TempStr := TempStr + ']';
      end;
      //Add arrow indicator - point right for visiting team
      TempStr := TempStr + FIELDARROWLEFT;
    end
    else TempStr := ' ';
    OutStr := TempStr;
  end

  //Weather related functions; store current league and home Stats ID in global variables to prevent
  //separate queries for each variable in Weather template
  else if (SymbolName = '$Weather_Stadium_Name') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := CurrentWeatherForecastRec.Weather_Stadium_Name;
  end
  else if (SymbolName = '$Weather_Current_Temperature') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := CurrentWeatherForecastRec.Weather_Current_Temperature;
  end
  else if (SymbolName = '$Weather_High_Temperature') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := CurrentWeatherForecastRec.Weather_High_Temperature;
  end
  else if (SymbolName = '$Weather_Low_Temperature') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := CurrentWeatherForecastRec.Weather_Low_Temperature;
  end
  else if (SymbolName = '$Weather_Current_Conditions') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := CurrentWeatherForecastRec.Weather_Current_Conditions;
  end
  else if (SymbolName = '$Weather_Forecast_Icon') then
  begin
    if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
       (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := CurrentWeatherForecastRec.Weather_Forecast_Icon;
  end;

  OutStr2 := OutStr;

  //Check if need to force to upper case
  if (ForceUpperCase) then OutStr2 := ANSIUpperCase(OutStr2);

  //Return
  GetValueOfSymbol := OutStr2;
end;

//Function to pull weather forecast data for the home stadium
function GetWeatherForecastData(League: String; HomeStatsID: LongInt): WeatherForecastRec;
var
  OutRec: WeatherForecastRec;
begin
  try
    //Run stored procedure to get weather data
    with dmMain.WeatherQuery do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add ('sp_GetWeatherData ' + QuotedStr(League) + ',' + IntToStr(HomeStatsID));
      Active := TRUE;
      if (RecordCount > 0) then
      begin
        OutRec.Weather_Stadium_Name := FieldByName('Weather_Stadium_Name').AsString;
        OutRec.Weather_Current_Temperature := FieldByName('Weather_Current_Temperature').AsString;
        OutRec.Weather_High_Temperature := FieldByName('Weather_High_Temperature').AsString;
        OutRec.Weather_Low_Temperature := FieldByName('Weather_Low_Temperature').AsString;
        OutRec.Weather_Current_Conditions := FieldByName('Weather_Current_Conditions').AsString;
        OutRec.Weather_Forecast_Icon := FieldByName('Weather_Forecast_Icon').AsString;
      end;
      Active := FALSE;
    end;
  except
  end;
  GetWeatherForecastData := OutRec;
end;

end.
