object AsRunLogViewerDlg: TAsRunLogViewerDlg
  Left = 440
  Top = 232
  Width = 941
  Height = 543
  Caption = 'As-Run Log Entry Viewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object AsRunLogGrid: TtsDBGrid
    Left = 10
    Top = 16
    Width = 903
    Height = 425
    CellSelectMode = cmNone
    CheckBoxStyle = stCheck
    ColMoving = False
    Cols = 4
    ColSelectMode = csNone
    DatasetType = dstStandard
    DataSource = dmMain.dsAsRunLog
    DefaultRowHeight = 18
    ExactRowCount = True
    ExportDelimiter = ','
    FieldState = fsCustomized
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    GridMode = gmListBox
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 18
    HeadingParentFont = False
    ParentFont = False
    ParentShowHint = False
    RowChangedIndicator = riAutoReset
    RowMoving = False
    RowSelectMode = rsSingle
    ShowHint = False
    TabOrder = 0
    Version = '2.20.26'
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    DataBound = True
    ColProperties = <
      item
        DataCol = 1
        FieldName = 'StationID'
        Col.FieldName = 'StationID'
        Col.Heading = 'Station ID'
        Col.Width = 81
        Col.AssignedValues = '?'
      end
      item
        DataCol = 2
        FieldName = 'LogEntryID'
        Col.FieldName = 'LogEntryID'
        Col.Heading = 'Entry ID'
        Col.Width = 68
        Col.AssignedValues = '?'
      end
      item
        DataCol = 3
        FieldName = 'EntryDateTime'
        Col.FieldName = 'EntryDateTime'
        Col.Heading = 'Date/Time'
        Col.Width = 167
        Col.AssignedValues = '?'
      end
      item
        DataCol = 4
        FieldName = 'LogEntry'
        Col.FieldName = 'LogEntry'
        Col.Heading = 'Log Entry'
        Col.Width = 581
        Col.AssignedValues = '?'
      end>
  end
  object BitBtn1: TBitBtn
    Left = 409
    Top = 452
    Width = 89
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 12
    Top = 451
    Width = 129
    Height = 41
    Caption = 'Export Data'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BitBtn2Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
      333333333333337FF3333333333333903333333333333377FF33333333333399
      03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
      99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
      99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
      03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
      33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
      33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
      3333777777333333333333333333333333333333333333333333}
    NumGlyphs = 2
  end
end
