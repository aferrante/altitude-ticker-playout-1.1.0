// Rev: 03/09/06  18:40  M. Dilworth  Video Design Software Inc.
// Modified 10/25/13 to support Icon Station for Altitude Sports
unit EngineIntf;

// Template sponsor types
// 1 = Sponsor with tagline
// 2 = Full-page sponsor
// 3 = Lower-right sponsor
// 4 = Clear lower-right sponsor

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    ACKReceivedLEDTimer: TTimer;
    EnginePort: TClientSocket;
    EngineCOMPort: TApdComPort;
    EngineCOMPortPacket: TApdDataPacket;
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure FormActivate(Sender: TObject);
    procedure ACKReceivedLEDTimerTimer(Sender: TObject);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure EngineCOMPortPortOpen(Sender: TObject);
    procedure EngineCOMPortPortClose(Sender: TObject);
    procedure EngineCOMPortPacketStringPacket(Sender: TObject; Data: String);
  private
    { Private declarations }
    function ProcessStyleChips(CmdStr: String): String;
  public
    { Public declarations }
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    function GetMixedCase(InStr: String): String;
    function FormatTimeString (InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function GetTeamMnemonic (LeagueCode: String; STTeamCode: String): String;
    function GetTeamBasename (LeagueCode: String; STTeamCode: String): String;
    function LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
    //Function to run stored procedures to get game information
    procedure UpdateTickerRecordDataFromDB(PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
    function GetTeamDisplayInfo2(League: String; TeamMnemonic: String): TeamDisplayInfoRec;
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function CheckForActiveSponsorLogo: Boolean;
    procedure WriteToCommandLog (CommandString: String);
    function GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
    function LastEntryIsSponsor: Boolean;

    //Added for Altitude Sports
    procedure InitTicker(BackplateIn: Boolean);
    function LoadTempTemplateCommands(TemplateID: SmallInt): Boolean;
    //Function to send updated data to Icon Station text/image fields
    function GetLayerUpdateString(LayoutName: String; FieldCount: SmallInt; RegionName, FieldName, FieldText: Array of String): String;
    //Procedure to load layer
    procedure LoadIconStationLayer(LayerName, TemplateLayer: String);
    //General procedure to send command to Icon Station
    procedure SendCommandToIconStation(CmdStr: String);
    procedure UnloadLayer(TemplateLayer: String);

  end;

const
  SOT: String[1] = #1;
  ETX: String[1] = #3;
  EOT: String[1] = #4;
  GS: String[1] = #29;
  RS: String[1] = #30;
  NAK: String[1] = #21; //Used as No-Op
  EOB: String[1] = #23; //Send at end of last record; will cause SS 11 to be sent by engine
  SingleLineWinningLarge: String[1] = #180;
  SingleLineLosingLarge: String[1]  = #181;
  SingleLineWinningSmall: String[1]  = #182;
  SingleLineLosingSmall: String[1]  = #183;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;
  //Boolean to select whether or not the data packet is used to trigger the next graphic

implementation

uses Main, //Main form
     DataModule,
     GameDataFunctions,
     LoggingFunctionsModule, CrispinInterfaceModule;

{$R *.DFM}
//Imit
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

//These are the graphics engine control procedures

////////////////////////////////////////////////////////////////////////////////
// ADDED FOR ALTITUDE SPORTS
////////////////////////////////////////////////////////////////////////////////
//Generric procedure to send command to Icon Station
procedure TEngineInterface.SendCommandToIconStation(CmdStr: String);
begin
  if (EngineParameters.Enabled) AND (SocketConnected) then
  begin
    //Check if COM Port
    if (EngineParameters.UseSerial) then
    begin
      EngineCOMPort.PutString(CmdStr);
      if (CommandLoggingEnabled) then LoggingFunctions.WriteToCommandLog(CmdStr);
    end
    //Check for TCP/IP port
    else begin
      EnginePort.Socket.SendText(CmdStr);
      if (CommandLoggingEnabled) then LoggingFunctions.WriteToCommandLog(CmdStr);
    end;
  end;
end;

//Procedure to bring ticker base template IN
procedure TEngineInterface.InitTicker(BackplateIn: Boolean);
var
  j: SmallInt;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  CmdStr: String;
begin
  //Always clear ticker
  if (UseKillAllCommandForClear) then
  begin
    CmdStr := 'T\14\' + IconStationTemplateLayer + '\\';
    SendCommandToIconStation(CmdStr + #13#10);
  end
  else begin
    LoadTempTemplateCommands(TICKER_OUT_TEMPLATE);

    if (Temporary_Template_Commands_Collection.Count > 0) then
    begin
      for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
      begin
        TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
        if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
        begin
          CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
          SendCommandToIconStation(CmdStr + #13#10);
        end;
      end;
    end;
  end;

  //Clear out sponsor info
  CurrentSponsorInfo.CurrentSponsorLogoName := '';
  CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
  CurrentSponsorInfo.CurrentSponsorTemplate := -99;
  CurrentSponsorInfo.Tagline_Top := '';
  CurrentSponsorInfo.Tagline_Bottom := '';

  //Bring backplate back in if enabled
  if (BackplateIn) then
  begin
    //Always clear ticker
    LoadTempTemplateCommands(TICKER_IN_TEMPLATE);

    if (Temporary_Template_Commands_Collection.Count > 0) then
    begin
      for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
      begin
        TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
        if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
        begin
          CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
          SendCommandToIconStation(CmdStr + #13#10);
        end;
      end;
    end;
  end;
end;

//Procedure to load layer
procedure TEngineInterface.LoadIconStationLayer(LayerName, TemplateLayer: String);
var
  CmdStr: String;
begin
  CmdStr := 'T\7\' + Trim(LayerName) + '/\' + Trim(TemplateLayer) + '\\';
  SendCommandToIconStation(CmdStr + #13#10);
end;

//Procedure to unload layer
procedure TEngineInterface.UnloadLayer(TemplateLayer: String);
var
  CmdStr: String;
begin
  CmdStr := 'T\14\' + Trim(TemplateLayer) + '\\';
  SendCommandToIconStation(CmdStr + #13#10);
end;

//Procedure to form region update command to send to Icon Station
function TEngineInterface.GetLayerUpdateString(LayoutName: String; FieldCount: SmallInt; RegionName, FieldName, FieldText: Array of String): String;
var
  i: SmallInt;
  OutStr: Array[0..19] of String;
  CmdStr: String;
begin
  //Check for valid array
  if (FieldCount > 0)   then
  begin
    for i := 0 to FieldCount-1 do OutStr[i] := FieldText[i];
    //Do substitutions for XML escape sequences
    for i := 0 to FieldCount-1 do
    begin
      OutStr[i] := StringReplace(OutStr[i], '&', '&amp;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '"', '&quot;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '''', '&apos;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '<', '&lt;', [rfReplaceAll]);
      OutStr[i] := StringReplace(OutStr[i], '>', '&gt;', [rfReplaceAll]);
    end;

    //Build command string
    CmdStr :=
      'I\42\<LayoutTags><LayoutName>' + Trim(Layoutname) + '</LayoutName>';
      for i := 0 to FieldCount-1 do
      begin
        if (Trim(RegionName[i]) <> '') then
        begin
          CmdStr := CmdStr +
            '<Region><Name>' + Trim(RegionName[i]) + '</Name>' +
            '<Tag><Name>' + Trim(FieldName[i]) + '</Name>' +
            '<Text>' + OutStr[i] + '</Text>' +
            '</Tag></Region>';
        end;
      end;
      CmdStr := CmdStr + '</LayoutTags>\\';
  end;
  //Return
  GetLayerUpdateString := CmdStr;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

//Function to take the team mnmonic and return the team name
function TEngineInterface.GetTeamDisplayInfo2(LEague: String; TeamMnemonic: String): TeamDisplayInfoRec;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to team record type
  OutRec: TeamDisplayInfoRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(TeamMnemonic)) AND
//         (Trim(TeamRecPtr^.League) = Trim(League))then
      begin
//        OutRec.DisplayMnemonic := TeamRecPtr^.DisplayName1;
//        OutRec.Displayname := TeamRecPtr^.DisplayName2;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Team_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetTeamDisplayInfo2 := OutRec;
end;

//Function to take the league code and Sportsticker team code, and return the team menmonic
function TEngineInterface.GetTeamMnemonic(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = LeagueCode) then
//      begin
//        OutStr := Trim(TeamRecPtr^.TeamMnemonic);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    Until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamMnemonic := OutStr;
end;

//Function to take the league code and Sportsticker team code, and return the team base name
function TEngineInterface.GetTeamBaseName(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = Trim(LeagueCode)) then
//      begin
//        OutStr := Trim(TeamRecPtr^.BaseName);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamBaseName := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Function to take 4-digit clock time string from Datamart and format as "mm:ss"
function TEngineInterface.FormatTimeString (InStr: String): String;
var
  OutStr: String;
begin
  OutStr := '';
  InStr := Trim(InStr);
  Case Length(InStr) of
    0: Begin
         OutStr := '';
       end;
    1: Begin
         OutStr := ':0' + InStr[1];
       end;
    2: Begin
         OutStr := ':' + InStr[1] + InStr[2];
       end;
    3: Begin
         OutStr := InStr[1] + ':' + InStr[2] + InStr[3];
       end;
    4: Begin
         OutStr := InStr[1] + InStr[2] + ':' + InStr[3] + InStr[4];
       end;
  end;
  FormatTimeString := OutStr;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Temporary_Fields_Collection.Clear;
  Temporary_Fields_Collection.Pack;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        OutRec.Engine_Template_ID := TemplateRecPtr^.Engine_Template_ID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.HideWebsiteURL := TemplateRecPtr^.HideWebsiteURL;
        OutRec.ShowLeagueChip := TemplateRecPtr^.ShowLeagueChip;
        OutRec.Template_Layer_Name := TemplateRecPtr^.Template_Layer_Name;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
        Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Engine_Field_ID := TemplateFieldsRecPtr^.Engine_Field_ID;
        Field_Delay := TemplateFieldsRecPtr^.Field_Delay;
        Layout_Region_Name := TemplateFieldsRecPtr^.Layout_Region_Name;
        Layout_Field_Name := TemplateFieldsRecPtr^.Layout_Field_Name;
        Field_Is_Crawl := TemplateFieldsRecPtr^.Field_Is_Crawl;
        Crawl_Pad_Leading := TemplateFieldsRecPtr^.Crawl_Pad_Leading;
        Crawl_Pad_Trailing := TemplateFieldsRecPtr^.Crawl_Pad_Trailing;
      end;
      if (Temporary_Fields_Collection.Count <= 100) then
      begin
        //Add to collection
        Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
        Temporary_Fields_Collection.Pack;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

//Added for Altitude Sports
//Procedure to load temporary collection for template commands (for current template)
function TEngineInterface.LoadTempTemplateCommands(TemplateID: SmallInt): Boolean;
var
  i: SmallInt;
  TemplateCommandRecPtr, NewTemplateCommandRecPtr: ^TemplateCommandRec;
  FoundMatch: Boolean;
begin
  if (Template_Commands_Collection.Count > 0) then
  begin
    FoundMatch := FALSE;
    //Clear the existing collection
    Temporary_Template_Commands_Collection.Clear;
    Temporary_Template_Commands_Collection.Pack;
    //Load the matching data
    for i := 0 to Template_Commands_Collection.Count-1 do
    begin
      TemplateCommandRecPtr := Template_Commands_Collection.At(i);
      if (TemplateCommandRecPtr^.Template_ID = TemplateID) then
      begin
        FoundMatch := TRUE;
        GetMem (NewTemplateCommandRecPtr, SizeOf(TemplateCommandRec));
        With NewTemplateCommandRecPtr^ do
        begin
          Template_ID := TemplateCommandRecPtr^.Template_ID;
          Command_Type := TemplateCommandRecPtr^.Command_Type;
          Command_Index := TemplateCommandRecPtr^.Command_Index;
          Command_Text := TemplateCommandRecPtr^.Command_Text;
        end;
        if (Temporary_Template_Commands_Collection.Count <= 500) then
        begin
          //Add to collection
          Temporary_Template_Commands_Collection.Insert(NewTemplateCommandRecPtr);
          Temporary_Template_Commands_Collection.Pack;
        end;
      end;
    end;
  end;
  LoadTempTemplateCommands := FoundMatch;
end;

//Utility function to strip off first character in string
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end
  else OutStr := InStr;
  StripPrefix := OutStr;
end;

//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

function GetLastName(FullName: String): String;
var
  i: SmallInt;
  OutStr: String;
  Cursor: SmallInt;
begin
  OutStr := FullName;
  Cursor := Pos(',', FullName);
  if (Cursor > 1) then
  begin
    OutStr := '';
    for i := 1 to Cursor-1 do
      OutStr := OutStr + FullName[i];
  end;
  GetLastName := OutStr;
end;

//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR (CurrentEntryLeague = 'IL') OR
       (CurrentEntryLeague = 'ML') then
      CurrentEntryLeague := 'MLB';

    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    {
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR (NextEntryLeague = 'ML') OR (NextEntryLeague = 'IL') then
        NextEntryLeague := 'MLB';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 4) OR (CollectionItemCounter = Ticker_Collection.Count*3);
    }

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 4 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 4 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to update data for a specific record from the ticker playlist; called for each record
procedure TEngineInterface.UpdateTickerRecordDataFromDB(PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  SQLString: String;
begin
  try
    dmMain.Query3.Close;
    dmMain.Query3.SQL.Clear;
    SQLString := 'SELECT * FROM Ticker_Elements WHERE Playlist_ID = '+ FloatToStr(PlaylistID) +
      ' AND Event_GUID = ' + QuotedStr(GUIDToString(EventGUID));
    dmMain.Query3.SQL.Add (SQLString);
    dmMain.Query3.Open;
    if (dmMain.Query3.RecordCount > 0) then
    begin
      //Don;t update if fantasy template - will overwrite stat index
      if ((dmMain.Query3.FieldByName('Template_ID').AsInteger MOD 1000) >= FantasyStartingBaseTemplateID) AND
         ((dmMain.Query3.FieldByName('Template_ID').AsInteger MOD 1000) <= FantasyEndingBaseTemplateID) then
      begin
        Exit;
      end
      else begin
        TickerRecPtr := Ticker_Collection.At(TickerRecordIndex);
        With TickerRecPtr^ do
        begin
          Enabled := dmMain.Query3.FieldByName('Enabled').AsBoolean;
          League := dmMain.Query3.FieldByName('League').AsString;
          Subleague_Mnemonic_Standard := dmMain.Query3.FieldByName('Subleague_Mnemonic_Standard').AsString;
          Mnemonic_LiveEvent := dmMain.Query3.FieldByName('Mnemonic_LiveEvent').AsString;
          //Load the user-defined text fields
          for i := 1 to 50 do
            UserData[i] := dmMain.Query3.FieldByName('UserData_' + IntToStr(i)).AsString;
          StartEnableDateTime := dmMain.Query3.FieldByName('StartEnableTime').AsDateTime;
          EndEnableDateTime := dmMain.Query3.FieldByName('EndEnableTime').AsDateTime;
          DwellTime := dmMain.Query3.FieldByName('DwellTime').AsInteger;
        end;
      end;
    end;
    dmMain.Query3.Close;
  except
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
    begin
      LoggingFunctions.WriteToSQLLog(StationIDForLogs, 111, IntToStr(TickerRecordIndex));
      WriteToErrorLog('Error occurred while trying to update data for ticker playlist entry #' + IntToStr(TickerRecordIndex));
    end;

  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if ((TickerRecPtr^.Template_ID = 1) OR (TickerRecPtr^.Template_ID = 2)) AND
         (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) AND
         (TickerRecPtr^.Enabled = TRUE) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

//Function to take a template ID and return the alternate template ID for 1-line vs.
//2-line mode
function TEngineInterface.GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := -1;
  if (Template_Defs_Collection.Count > 0) then
  begin
    //Walk through templates to find alternate mode ID
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      //Check for match between ticker element template ID and entry in template defs
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
      begin
        //Alternate template ID found, so re-assign template ID
        if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
        begin
          OutVal := TemplateDefsRecPtr^.AlternateModeTemplateID;
        end;
      end;
    end;
  end;
  GetAlternateTemplateID := OutVal;
end;

function TEngineInterface.LastEntryIsSponsor: Boolean;
var
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check if last entry is sponsor
  TickerRecPtr := Ticker_Collection.At(Ticker_Collection.Count-1);
  if (TickerRecPtr^.Template_ID = 1) OR (TickerRecPtr^.Template_ID = 51) OR
     (TickerRecPtr^.Template_ID = 101) OR (TickerRecPtr^.Template_ID = 151) then
    OutVal := TRUE;
  //Return
  LastEntryIsSponsor := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler for ACK received from Icon Station
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  //Enable the command delay timer - disabled for Altitude
  if (Length(Data) >= 1) AND (Pos('*', ANSIUpperCase(Data)) > 0) then
  begin
    if (USEDATAPACKET) AND (RunningTicker) then
    begin
      MainForm.ApdStatusLight5.Lit := TRUE;
      //Light ACK received indicator
      ACKReceivedLEDTimer.Enabled := TRUE;
      //Write to command log
      if (CommandLoggingEnabled) then
      begin
        WriteToCommandLog('Data Packet received: ' + Data);
      end;
    end;

    //Send ACK to Crispin automation
    if (CrispinComPort_Enabled) then CrispinInterface.CrispinComPort.PutString(Data);

    //Log ACK command to GUI memo box
    //if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
    //MainForm.AutomationLogMemoBox.Lines.Add(DateTimeToStr(Now) + ': Sent ACK (*) to automation');
  end
  else begin
  end;
end;

//Handler for COM port atring packet - serial control of Icon Station
procedure TEngineInterface.EngineCOMPortPacketStringPacket(Sender: TObject; Data: String);
begin
  //Enable the command delay timer - disabled for Altitude
  if (Length(Data) >= 1) AND (Pos('*', ANSIUpperCase(Data)) > 0) then
  begin
    if (USEDATAPACKET) AND (RunningTicker) then
    begin
      MainForm.ApdStatusLight5.Lit := TRUE;
      //Light ACK received indicator
      ACKReceivedLEDTimer.Enabled := TRUE;
      //Write to command log
      if (CommandLoggingEnabled) then
      begin
        WriteToCommandLog('Data Packet received: ' + Data);
      end;
    end;

    //Send ACK to Crispin automation
    //if (CrispinComPort.Open) then CrispinComPort.PutString(Data);
    //Log ACK command to GUI memo box
    if (Debug) then
    begin
      if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
      MainForm.AutomationLogMemoBox.Lines.Add(DateTimeToStr(Now) + ': Received ACK (*) from Icon Station');
    end;  
  end
  else begin
  end;
end;

//Timer to extinguish ACK received indicator
procedure TEngineInterface.ACKReceivedLEDTimerTimer(Sender: TObject);
begin
  ACKReceivedLEDTimer.Enabled := FALSE;
  MainForm.ApdStatusLight5.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      TickerCommandDelayTimer.Enabled := FALSE;
      //Send next record
      SendTickerRecord(TRUE, 0);
    end;
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      JumpToNextTickerRecordTimer.Enabled := FALSE;
      //Send next record
      SendTickerRecord(TRUE, 0);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j,k: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtrNew: ^BreakingNewsRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OkToGo: Boolean;
  UseFieldDelayTimer: Boolean;
  TempTickerEntryIndex: SmallInt;
  TokenCounter: SmallInt;
  SymbolValueData: SymbolValueRec;

  //Added for Altitude Sports
  Layout_Region_Name: Array[0..19] of String;
  Layout_Field_Name: Array[0..19] of String;
  Layout_Field_Text: Array[0..19] of String;
  Layout_Field_Count: SmallInt;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  League_Chip_Region_Name: Array[0..1] of String;
  League_Chip_Field_Name: Array[0..1] of String;
  League_Chip_Field_Text: Array[0..1] of String;
  Disable_Template_Out_Commands: Boolean;

begin
  //Init flags
  USEDATAPACKET := TRUE;
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Added for Altitude Sports
  Disable_Template_Out_Commands := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Init packet timeout timer to disabled
  TickerPacketTimeoutTimer.Enabled := FALSE;

  //Set looping mode as required
  Case TickerDisplayMode of
    //1x through for 1-lime, 2-line
    1,3: LoopTicker := FALSE;
    //Continuous loop for 1-lime, 2-line
    2,4: LoopTicker := TRUE;
  end;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Set temporary ticker rec pointer - used for segment mode breaking news
    if (NextRecord) then
    begin
      if (CurrentTickerEntryIndex < Ticker_Collection.Count-1) then
      begin
        TempTickerEntryIndex := CurrentTickerEntryIndex+1;
        TickerRecPtrNew := Ticker_Collection.At(TempTickerEntryIndex);
      end
      else
        TickerRecPtrNew := Ticker_Collection.At(0);
    end;

    //Make sure we haven't dumped put
    if (TickerAbortFlag = FALSE) AND
      ((Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          try
            //Reload the Ticker collection in case it has been modified; clear flag to indicate
            //it's not the first time through
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            if (Ticker_Collection.Count > 0) then CurrentTickerEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
            begin
              LoggingFunctions.WriteToSQLLog(StationIDForLogs, 308, '');
              WriteToErrorLog('Error occurred while trying to reload main ticker collection (1)');
            end;
          end;
          //Check time window and enable
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          //Make sure record is enabled and within time window
          j := 0;
          While ((TickerRecPtr^.Enabled = FALSE) OR NOT ((TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < Ticker_Collection.Count) do
          begin
            Inc(CurrentTickerEntryIndex);
            //Set pointer
            TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
            if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (LoopTicker = TRUE) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                CurrentTickerEntryIndex := 0;
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                begin
                  LoggingFunctions.WriteToSQLLog(StationIDForLogs, 309, '');
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection (2)');
                end;
              end;
            end
            else if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = FALSE) then
            begin
              //Abort the ticker and exit this procedure
              MainForm.AbortCurrentEvent;
              MainForm.ResetTickerToTop;
              PacketEnableTimer.Enabled := TRUE;
              RunningTicker := FALSE;
              //Dump out
              Exit;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= Ticker_Collection.Count) then
          begin
            OKToGo := FALSE;
            //Abort the ticker and exit this procedure
            MainForm.AbortCurrentEvent;
            MainForm.ResetTickerToTop;
            PacketEnableTimer.Enabled := TRUE;
            RunningTicker := FALSE;
            //Dump out
            Exit;
          end;
        end
        //Not last record, so increment Ticker playout collection object index
        else if (CurrentTickerEntryIndex < Ticker_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentTickerEntryIndex);
            if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (LoopTicker = TRUE) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                CurrentTickerEntryIndex := 0;
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                begin
                  LoggingFunctions.WriteToSQLLog(StationIDForLogs, 310, '');
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection (3)');
                end;
              end;
            end
            //Abort ticker if not looping
            else if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = FALSE) then
            begin
              //Abort the ticker and exit this procedure
              MainForm.AbortCurrentEvent;
              MainForm.ResetTickerToTop;
              PacketEnableTimer.Enabled := TRUE;
              RunningTicker := FALSE;
              //Dump out
              Exit;
            end;
            TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
            Inc (j);
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          Until ((TickerRecPtr^.Enabled = TRUE) AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now)) OR (j >= Ticker_Collection.Count);
          //Set flag if no enabled records found
          //if (j >= Ticker_Collection.Count) then
          if (j > Ticker_Collection.Count) then
          begin
            OKToGo := FALSE;
            //Abort the ticker and exit this procedure
            MainForm.AbortCurrentEvent;
            MainForm.ResetTickerToTop;
            PacketEnableTimer.Enabled := TRUE;
            RunningTicker := FALSE;
            //Dump out
            Exit;
          end;
        end
        //At end and not looping, so clear screen
        else if (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          MainForm.AbortCurrentEvent;
          MainForm.ResetTickerToTop;
          PacketEnableTimer.Enabled := TRUE;
          RunningTicker := FALSE;
          //Dump out
          Exit;
        end;

        //Set last segment heading - used for segment mode breaking news
        LastSegmentHeading := TickerRecPtr^.League;

        //Clear ticker if at last collection obejct and not looping
        if (CurrentTickerEntryIndex > Ticker_Collection.Count-1) AND
           (LoopTicker = FALSE) then
        begin
          MainForm.AbortCurrentEvent;
          MainForm.ResetTickerToTop;
          RunningTicker := FALSE;
          PacketEnableTimer.Enabled := TRUE;
          //Dump out
          Exit;
        end;
        //Disable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentTickerEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
        //Update the data from the database - called for each record
        UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);

        //If in ticker mode, get game data & process to check game states
        //Get game data if template requires it and in ticker mode
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
          except
            if (ErrorLoggingEnabled = True) then
            begin
              LoggingFunctions.WriteToSQLLog(StationIDForLogs, 113, '');
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.StateForInProgressCheck <> TemplateInfo.RequiredGameState) AND
           //Special case for postponed, delayed or suspended MLB games
           (not(((CurrentGameData.State = -1) OR (CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR
                 (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = CANCELLED)) AND
                 (TemplateInfo.RequiredGameState = INPROGRESS))) and
           (NEXTRECORD = TRUE) then
        repeat
          Inc(CurrentTickerEntryIndex);
          if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = TRUE) then
          begin
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            CurrentTickerEntryIndex := 0;
          end
          else if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = FALSE) then
          begin
            //Abort the ticker and exit this procedure
            MainForm.AbortCurrentEvent;
            MainForm.ResetTickerToTop;
            PacketEnableTimer.Enabled := TRUE;
            RunningTicker := FALSE;
            //Dump out
            Exit;
          end;
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
          TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Use full game data
              CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
            except
              if (ErrorLoggingEnabled = True) then
              begin
                LoggingFunctions.WriteToSQLLog(StationIDForLogs, 114, '');
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
              end;
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.State := UNDEFINED;
         //Version 2.1.0.0 Bug corrected that resulted in all templates being displayed for games that were delayed, cancelled, etc.
          Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.StateForInProgressCheck = TemplateInfo.RequiredGameState) OR
                //Special case for postponed, delayed or suspended MLB games
                (((CurrentGameData.State = -1) OR (CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR
                  (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = CANCELLED)) AND
                  (TemplateInfo.RequiredGameState = INPROGRESS))) AND
                ((TickerRecPtr^.Enabled = TRUE) AND
                (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

        //Set flag
        USEDATAPACKET := TRUE;

        //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        //Check for value of -1 -> Use data packet for crawling template
        //If sponsor logo dwell is specified and Template ID = 1, it's a sponsor logo with tagline, so use that for the dwell time
        if (TickerRecPtr^.SponsorLogo_Dwell > 0) AND  ((TemplateInfo.TemplateSponsorType = 1) OR (TemplateInfo.TemplateSponsorType = 2)) AND
           (TickerRecPtr^.Template_ID = 1) then
        begin
          if (FullSponsorPagesEnabled) then
          begin
            TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
            TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 60000;
          end
          else begin
            if (TickerRecPtr^.DwellTime > 2000) then
            begin
              TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
              TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
            end
            else begin
              TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
              if (TickerRecPtr^.DwellTime = 1) then
                TickerPacketTimeoutTimer.Interval := 90000
              else
                TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
            end;
          end;
          //Set the current sponsor logo name, template, taglines & dwell
          CurrentSponsorInfo.CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
          CurrentSponsorInfo.CurrentSponsorLogoDwell := TickerRecPtr^.SponsorLogo_Dwell;
          CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
          CurrentSponsorInfo.Tagline_Top := TickerRecPtr^.UserData[1];
          CurrentSponsorInfo.Tagline_Bottom := TickerRecPtr^.UserData[2];
        end
        //If lower-right sponsor template, sponsor logo dwell does not need to be specified
        else if (TemplateInfo.TemplateSponsorType = 3) then
        begin
          //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
          CurrentSponsorInfo.CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
          CurrentSponsorInfo.CurrentSponsorLogoDwell := TickerRecPtr^.SponsorLogo_Dwell;
          CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
          CurrentSponsorInfo.Tagline_Top := '';
          CurrentSponsorInfo.Tagline_Bottom := '';
          TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
        end
        //If clear lower-right sponsor template, clear the name
        else if (TemplateInfo.TemplateSponsorType = 4) then
        begin
          //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
          CurrentSponsorInfo.CurrentSponsorLogoName := '';
          CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
          CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
          CurrentSponsorInfo.Tagline_Top := '';
          CurrentSponsorInfo.Tagline_Bottom := '';
          TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
        end
        //Not a sponsor logo
        else if (TickerRecPtr^.DwellTime > 2000) then
        begin
          TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
        end
        else begin
          TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
          if (TickerRecPtr^.DwellTime = 1) then
            TickerPacketTimeoutTimer.Interval := 90000
          else
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
        end;

        //Check to see if active sponsor logo needs to be cleared for main sponsor templates
        //if (CheckForActiveSponsorLogo = FALSE) then CurrentSponsorInfo.CurrentSponsorLogoName := '';

        //Add the fields from the temporary fields collection
        //Set field count
        Layout_Field_Count := Temporary_Fields_Collection.Count;
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);

            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add previx and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    SymbolValueData := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents,
                      CurrentTickerEntryIndex, CurrentGameData, TickerDisplayMode);
                    TempStr := SymbolValueData.SymbolValue;
                    //If data not received, skip record
                    if (SymbolValueData.OKToDisplay = FALSE) then
                    begin
                      JumpToNextTickerRecordTimer.Enabled := TRUE;
                      Exit;
                    end;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;

                    //Pad if crawl text
                    if (TemplateFieldsRecPtr^.Field_Is_Crawl) then
                    begin
                      if (TemplateFieldsRecPtr^.Crawl_Pad_Leading > 0) then
                        for k := 1 to TemplateFieldsRecPtr^.Crawl_Pad_Leading do
                          TempStr := #160 + TempStr;
                      if (TemplateFieldsRecPtr^.Crawl_Pad_Trailing > 0) then
                        for k := 1 to TemplateFieldsRecPtr^.Crawl_Pad_Trailing do
                          TempStr := TempStr + #160;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                    begin
                      LoggingFunctions.WriteToSQLLog(StationIDForLogs, 314, '');
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                    end;
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end;

                //Don't send update data for image fields
                if (TemplateFieldsRecPtr^.Field_Type <> FIELD_TYPE_SPONSOR_LOGO) and (TemplateFieldsRecPtr^.Field_Type <> FIELD_TYPE_TEAM_LOGO_1A) and
                   (TemplateFieldsRecPtr^.Field_Type <> FIELD_TYPE_TEAM_LOGO_2A) then
                begin
                  //Set values for layers
                  Layout_Region_Name[j] :=  TemplateFieldsRecPtr^.Layout_Region_Name;
                  Layout_Field_Name[j] :=  TemplateFieldsRecPtr^.Layout_Field_Name;
                  Layout_Field_Text[j] :=  TempStr;
                end;

                //Update any logos
                //Check for sponsor logo
                if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_SPONSOR_LOGO) then
                begin
                  try
                    with dmMain.LogoUpdateQuery do
                    begin
                      Close;
                      SQL.Clear;
                      SQL.Add('sp_UpdateSponsorLogo ' + QuotedStr(TempStr));
                      ExecSQL;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to update sponsor logo in SQL database');
                  end;
                end
                //Check for alert logo - left
                else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) then
                begin
                  try
                    with dmMain.LogoUpdateQuery do
                    begin
                      Close;
                      SQL.Clear;
                      SQL.Add('sp_UpdateAlertLogoLeft ' + QuotedStr(TempStr));
                      ExecSQL;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to update left-side alert logo in SQL database');
                  end;
                end
                //Check for alert logo - right
                else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) then
                begin
                  try
                    with dmMain.LogoUpdateQuery do
                    begin
                      Close;
                      SQL.Clear;
                      SQL.Add('sp_UpdateAlertLogoRight ' + QuotedStr(TempStr));
                      ExecSQL;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to update right-side alert logo in SQL database');
                  end;
                end;
              end;
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
          begin
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorInfo.CurrentSponsorLogoName);
            //Added SQL Log for V3.1
            LoggingFunctions.WriteToSQLAsRunLog(StationIDForLogs, 'Started display of sponsor logo: ' + CurrentSponsorInfo.CurrentSponsorLogoName);
          end;
        end;

        //Set data row indicator in Ticker grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
            MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
            begin
              LoggingFunctions.WriteToSQLLog(StationIDForLogs, 400, '');
              WriteToErrorLog('Error occurred while trying to reposition grid cursor to correct record');
            end;
          end;
        end;

        //Set the lineup text
        try
          //Get the lineup text
          try
            if (GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode).UseLineup = TRUE) then
            begin
              LineupData := GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode);
              //Version 2.0.0.5 Modified to reflect change to line-up/sponsor interaction
              if (Trim(LineupData.TextFields[1]) = 'NONE') then
                LineupData.TextFields[1] := ' ';
              //Set league text - done below
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get and set main ticker line-up text');
          end;
        except
          if (ErrorLoggingEnabled = True) then
          begin
            LoggingFunctions.WriteToSQLLog(StationIDForLogs, 316, '');
            WriteToErrorLog('Error occurred while trying to get and set main ticker line-up text');
          end;
        end;

        ///////////////////////////////
        // NEW CODE FOR ALTITUDE
        ///////////////////////////////
        //if first proofing page, bring in backplate
        if not (TickerbackplateIn) then
        begin
          TickerbackplateIn := TRUE;
          //Always show ticker backplate
          LoadTempTemplateCommands(TICKER_IN_TEMPLATE);

          if (Temporary_Template_Commands_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
            begin
              TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
              if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
              begin
                CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                SendCommandToIconStation(CmdStr + #13#10);
              end;
            end;
          end;
        end;

        //Send the new data
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then TickerPacketTimeoutTimer.Enabled := TRUE;
          //Send commands
          try
            //Update text fields
            CmdStr := GetLayerUpdateString(TemplateInfo.Template_Layer_Name, Layout_Field_Count, Layout_Region_Name, Layout_Field_Name, Layout_Field_Text);
            SendCommandToIconStation(CmdStr + #13#10);

            //If this is a new template, fire the commands to take the previous template out
            if (LastActiveTemplateID <> TemplateInfo.Template_ID) and (LastActiveTemplateID <> 0) and not (Disable_Template_Out_Commands) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(LastActiveTemplateID);

              //Fire the commands to take the old the template out
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_OUT) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end
            //Same template so fire commands to take just text fields out
            else if (LastActiveTemplateID <> 0) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(TemplateInfo.Template_ID);

              //Fire the commands to bring the template in
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_START) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end;

            //Update the league chip if it's different than the last league designation
            if (LineupData.TextFields[1] <> Last_League_Text) {and (LastActiveTemplateID <> 0)} and (TemplateInfo.ShowLeagueChip) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(LEAGUE_CHIP_TEMPLATE_ID);

              //Update league chip text
              League_Chip_Region_Name[0] := 'league';
              League_Chip_Field_Name[0] := 'Field1';
              League_Chip_Field_Text[0] := LineupData.TextFields[1];
              CmdStr := GetLayerUpdateString(TemplateInfo.Template_Layer_Name, 1, League_Chip_Region_Name, League_Chip_Field_Name, League_Chip_Field_Text);
              SendCommandToIconStation(CmdStr + #13#10);

              //Fire the commands to start the update
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_START) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;

              //Fire the commands to end the update
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_END) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;

              //Save new league name
              Last_League_Text := LineupData.TextFields[1];
            end;

            //Fire the commands to bring the template in
            //If this is a new template, fire the commands to bring the new template in
            if (LastActiveTemplateID <> TemplateInfo.Template_ID) then
            begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(TemplateInfo.Template_ID);

              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end
            //Same template so fire commands to take bring text fields in after update
            else begin
              //Added for Altitude Sports - get the template commands
              LoadTempTemplateCommands(TemplateInfo.Template_ID);

              //Fire the commands to bring the template in
              if (Temporary_Template_Commands_Collection.Count > 0) then
              begin
                for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
                begin
                  TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                  if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_UPDATE_END) then
                  begin
                    CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                    SendCommandToIconStation(CmdStr + #13#10);
                  end;
                end;
              end;
            end;

            //Bring the website URL in or out based on state of sponsor logo
            //Check if sponsor specified
            if (CurrentSponsorInfo.CurrentSponsorLogoName <> '') or (TemplateInfo.HideWebsiteURL = TRUE) then
              LoadTempTemplateCommands(WEBSITE_OUT_TEMPLATE)
            //No sponsor specified, so bring URL in
            else
              LoadTempTemplateCommands(WEBSITE_IN_TEMPLATE);
            //Send the commands
            if (Temporary_Template_Commands_Collection.Count > 0) then
            begin
              for j := 0 to Temporary_Template_Commands_Collection.Count-1 do
              begin
                TemplateCommandRecPtr := Temporary_Template_Commands_Collection.At(j);
                if (TemplateCommandRecPtr^.Command_Type = TEMPLATE_IN) then
                begin
                  CmdStr := 'T\7\/' + TemplateCommandRecPtr^.Command_Text + '\' + IconStationTemplateLayer + '\\';
                  SendCommandToIconStation(CmdStr + #13#10);
                end;
              end;
            end;
            
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Start timer to send next record
          //if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then TickerCommandDelayTimer.Enabled := TRUE;
          //Modified for Altitude to always use ticker command timer
          TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        end;
      end
      //OK to go was set to false
      else begin
        //Enable delay timer
        if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
      end;
    end;
    //Set last template ID
    LastActiveTemplateID := TemplateInfo.Template_ID;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
      begin
        LoggingFunctions.WriteToSQLLog(StationIDForLogs, 205, '');
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
      end;
    end;
  end;
  //Set dwell label value
  MainForm.DwellValue.Caption := IntToStr(TickerCommandDelayTimer.Interval);
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  TickerPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendTickerRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
    begin
      LoggingFunctions.WriteToSQLLog(StationIDForLogs, 206, '');
      WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
    end;
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  //MessageDlg('The connection to the graphics engine was lost. ' +
  //           'You will not be able to control the graphics engine. If you wish to try ' +
  //           'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
  //           'from the main program menu.', mtError, [mbOK], 0);
  MainForm.StatusBar.Color := clRed;
  MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Lost connection to graphics engine.';
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
    begin
      LoggingFunctions.WriteToSQLLog(StationIDForLogs, 207, '');
      WriteToErrorLog('Lost connection to ticker graphics engine. Please verify that the engine is running. This may also ' +
        'be due to problems with your network');
    end;
    if (RunningTicker) then
    begin
      //Abort
      MainForm.AbortCurrentEvent;
    end;
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  //MessageDlg('The connection to the graphics engine was lost. ' +
  //           'You will not be able to control the graphics engine. If you wish to try ' +
  //           'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
  //           'from the main program menu.', mtError, [mbOK], 0);
  MainForm.StatusBar.Color := clRed;
  MainForm.StatusBar.SimpleText := '[' + DateTimeToStr(Now) + '] ' + 'Error encountered with connection to graphics engine.';
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
    begin
      LoggingFunctions.WriteToSQLLog(StationIDForLogs, 208, IntToStr(ErrorCode));
      WriteToErrorLog('Error occurred with connection to ticker graphics engine. Please verify that the engine is running. This may also ' +
        'be due to problems with your network - Error Code:');
    end;
    if (RunningTicker) then
    begin
      //Abort
      MainForm.AbortCurrentEvent;
    end;
  end;
  ErrorCode := 0;
end;

//Functions for COM port
//COM port open
procedure TEngineInterface.EngineCOMPortPortOpen(Sender: TObject);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//COM port close
procedure TEngineInterface.EngineCOMPortPortClose(Sender: TObject);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  if (ErrorLoggingEnabled = True) then
  begin
    MessageDlg('The COM Port connection to the graphics engine was lost. ' +
               'You will not be able to control the graphics engine. If you wish to try ' +
               'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
               'from the main program menu.', mtError, [mbOK], 0);

    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with COM Port connection to ticker graphics engine');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\Comcast_Error_LogFiles') = FALSE) then
   begin
     CreateDir('c:\Comcast_Error_LogFiles');
     DirectoryStr := 'c:\Comcast_Error_LogFiles\';
   end;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Altitude_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\Altitude_AsRun_LogFiles');
     DirectoryStr := 'c:\Altitude_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'AltitudeAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

procedure TEngineInterface.WriteToCommandLog (CommandString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   CommandLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(CommandLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Altitude_Command_LogFiles') = FALSE) then
       CreateDir('c:\Altitude_Command_LogFiles');
     DirectoryStr := 'c:\Altitude_Command_LogFiles\';
   end
   else DirectoryStr := CommandLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   CommandLogStr := TimeStr + '    ' + DateStr + '    ' + CommandString +
     '[TIME = ' + IntToStr(Hour) + ':' + IntToStr(Min) + ':' + IntToStr(Sec) +
     ':' + IntToStr(MSec) + ']';
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'AltitudeCommandLog' + DayStr + '.txt';
   AssignFile (CommandLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (CommandLogFile);
      try
         Writeln (CommandLogFile, CommandLogStr);      finally         CloseFile (CommandLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (CommandLogFile);      try         Readln (CommandLogFile, TestStr);      finally         CloseFile (CommandLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (CommandLogFile);         {Create a new logfile}         ReWrite (CommandLogFile);         try
            Writeln (CommandLogFile, CommandLogStr);         finally            CloseFile (CommandLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (CommandLogFile);         try
            Writeln (CommandLogFile, CommandLogStr);         finally            CloseFile (CommandLogFile);         end;      end;   end;end;

end.


