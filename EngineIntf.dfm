object EngineInterface: TEngineInterface
  Left = 508
  Top = 241
  Width = 286
  Height = 251
  Caption = 'Engine Interface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object AdTerminal1: TAdTerminal
    Left = 66
    Top = 8
    Width = 145
    Height = 73
    Active = False
    CaptureFile = 'APROTERM.CAP'
    ComPort = EngineCOMPort
    Scrollback = False
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -12
    Font.Name = 'Terminal'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 0
  end
  object EngineCOMPort: TApdComPort
    AutoOpen = False
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnPortClose = EngineCOMPortPortClose
    OnPortOpen = EngineCOMPortPortOpen
    Left = 224
    Top = 8
  end
  object PacketEnableTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = PacketEnableTimerTimer
    Left = 24
    Top = 56
  end
  object TickerCommandDelayTimer: TTimer
    Enabled = False
    OnTimer = TickerCommandDelayTimerTimer
    Left = 24
    Top = 94
  end
  object PacketIndicatorTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = PacketIndicatorTimerTimer
    Left = 56
    Top = 177
  end
  object TickerPacketTimeoutTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TickerPacketTimeoutTimerTimer
    Left = 24
    Top = 132
  end
  object JumpToNextTickerRecordTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = JumpToNextTickerRecordTimerTimer
    Left = 59
    Top = 94
  end
  object DisplayClockTimer: TTimer
    Enabled = False
    Interval = 10000
    Left = 24
    Top = 176
  end
  object ACKReceivedLEDTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = ACKReceivedLEDTimerTimer
    Left = 59
    Top = 134
  end
  object EnginePort: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = EnginePortConnect
    OnDisconnect = EnginePortDisconnect
    OnRead = EnginePortRead
    OnError = EnginePortError
    Left = 24
    Top = 16
  end
  object EngineCOMPortPacket: TApdDataPacket
    Enabled = True
    StartCond = scAnyData
    EndCond = [ecString]
    EndString = '\\'#13#10
    ComPort = EngineCOMPort
    PacketSize = 1
    OnStringPacket = EngineCOMPortPacketStringPacket
    Left = 224
    Top = 48
  end
end
