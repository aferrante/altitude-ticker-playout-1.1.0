unit CrispinInterfaceModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdPacket, OoMisc, AdPort, Globals;

type
  TCrispinInterface = class(TForm)
    CrispinCOMPort: TApdComPort;
    CrispinCOMPortPacket: TApdDataPacket;
    procedure CrispinCOMPortPortClose(Sender: TObject);
    procedure CrispinCOMPortPortOpen(Sender: TObject);
    procedure CrispinCOMPortPacketStringPacket(Sender: TObject;
      Data: String);
    procedure CrispinCOMPortPacketTimeout(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CrispinInterface: TCrispinInterface;

implementation

uses Main, LoggingFunctionsModule, EngineIntf;

{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////
// Handlers for Crispin Automation connection
////////////////////////////////////////////////////////////////////////////////
//COM Port disconnect
procedure TCrispinInterface.CrispinCOMPortPortClose(Sender: TObject);
begin
  MainForm.AutomationLED.Lit := FALSE;
end;

//COM Port connect
procedure TCrispinInterface.CrispinCOMPortPortOpen(Sender: TObject);
var
  i: SmallInt;
begin
  MainForm.AutomationLED.Lit := TRUE;
  //CrispinComPortPacket.Enabled := FALSE;
  //CrispinComPortPacket.Enabled := TRUE;
  //CrispinComPort.PutString('*' + #13#10);
end;

//Handler for string received from Crispin automation
procedure TCrispinInterface.CrispinCOMPortPacketStringPacket(Sender: TObject; Data: String);
var
  PlaylistName: String;
  NamePos, SaveNamePos: SmallInt;
  IDStr: String;
  IDPos: SmallInt;
  IDVal: LongInt;
  fs : TFormatSettings;
begin
  GetLocaleFormatSettings(0, fs);
  fs.ShortTimeFormat := 'hh:mm:ss';

  if (Debug) then
  begin
    if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
    MainForm.AutomationLogMemoBox.Lines.Add(DateToStr(Now) + ' ' + TimeToStr(Now, fs) + ': ' + 'Received data packet: ' + Trim(Data));
  end;

  //Check if automation monitoring enabled
  if not (DisableAutomationMonitoring) then
  begin
    //Modified to look for forward slash delimiter at start of salvo name
    NamePos := Pos(UpperCase('/TK'), Trim(UpperCase(Data)));
    //Increment to get past forward slash
    if (NamePos > 0) then
    begin
      Inc(NamePos);
      SaveNamePos := NamePos;
      PlaylistName := '';
      //Check for ticker command and pass-through not enabled
      if (NamePos > 0) and not (EnableAutomationPassThrough) then
      begin
        //Parse out to get layout name (will be ticker name)
        //while (Data[NamePos] <> '/') and (NamePos < Length(Data)) do
        while (Data[NamePos] <> '\') and (NamePos < Length(Data)) do
        begin
          PlaylistName := PlaylistName + Data[NamePos];
          Inc(NamePos);
        end;

        //Debug
        if (Debug) then
        begin
          if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
          MainForm.AutomationLogMemoBox.Lines.Add(DateToStr(Now) + ' ' + TimeToStr(Now, fs) + ': ' + 'Full Playlist Name: ' + PlaylistName);
        end;

        //Check to see if template ID is within range
        IDPos := SaveNamePos + Length(CrispinTickerCommandPrefix);
        //Parse out to get ID
        //while (Data[IDPos] <> '/') and (IDPos < Length(Data)) do
        while (Data[IDPos] <> '\') and (IDPos < Length(Data)) do
        begin
          IDStr := IDStr + Data[IDPos];
          Inc(IDPos);
        end;
        IDVal := StrToIntDef(IDStr, -1);

        //Debug
        if (Debug) then
        begin
          if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
          MainForm.AutomationLogMemoBox.Lines.Add(DateToStr(Now) + ' ' + TimeToStr(Now, fs) + ': ' + 'Playlist ID: ' + IDStr);
        end;

        //If value out of range, just pass command
        if (IDVal < StartingValidTickerID) or (IDVal > EndingValidTickerID) then
        begin
          //Send the command
          EngineInterface.SendCommandToIconStation(Data + #13#10);
          //Log command to GUI memo box
          if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
          MainForm.AutomationLogMemoBox.Lines.Add(DateTimeToStr(Now, fs) + ': Passing Out-of-Range Ticker Command' + Trim(Data));
        end
        //In valid range, so load and trigger
        else begin
          //Only trigger if not already running
          if not (RunningTicker) then
          begin
            //Load the playlist
            if (MainForm.LoadTickerPlaylistCollectionByName(PlaylistName)) then
            begin
              //Load the layer on the Icon Station
              EngineInterface.LoadIconStationLayer(IconStationLayerName, IconStationTemplateLayer);
              Sleep(1000);
              //Trigger the playlist
              MainForm.TriggerCurrentTicker;
              //Log command to GUI memo box
              if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
              MainForm.AutomationLogMemoBox.Lines.Add(DateTimeToStr(Now, fs) + ': Triggering Ticker ID: ' + PlaylistName);
            end
            //Playlist not found, so log error
            else begin
              if (ErrorLoggingEnabled = True) then
              begin
                LoggingFunctions.WriteToSQLLog(StationIDForLogs, 324, Data);
                EngineInterface.WriteToErrorLog('Matching playlist not found in database for ticker command received from Automation: ' + Data);
              end;
              //Debug
              if (Debug) then
              begin
                if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
                MainForm.AutomationLogMemoBox.Lines.Add(DateToStr(Now) + ' ' + TimeToStr(Now, fs) + ': ' + 'Matching playlist ID not found in SQL database');
              end;
            end;
          end;
        end;
      end
      //In pass through mode
      else if (EnableAutomationPassThrough) then
      begin
        //Send the command
        EngineInterface.SendCommandToIconStation(Data + #13#10);
        //Log command to GUI memo box
        if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
        MainForm.AutomationLogMemoBox.Lines.Add(DateTimeToStr(Now, fs) + ': Passing Ticker Command in Bypass Mode' + Trim(Data));
      end;
    end
    //Not a ticker command or outside of range, so just pass it to the Icon Station
    else begin
      //First , check to see if it's an unload command; if so, halt ticker
      if (Pos('T\14\', Trim(UpperCase(Data))) > 0) then
      begin
        MainForm.AbortCurrentEvent;
      end;

      //Log command to GUI memo box
      begin
        if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
        MainForm.AutomationLogMemoBox.Lines.Add(DateToStr(Now) + ' ' + TimeToStr(Now, fs) + ': ' + 'Passing Non-Ticker Command: ' + Trim(Data));
      end;

      //Send the command
      EngineInterface.SendCommandToIconStation(Data + #13#10);
    end;
  end;
  //Send ACK to Crispin automation
  if (CrispinComPort_Enabled) then
  begin
    CrispinComPort.PutString('*' + #13#10);
    if (Debug) then
    begin
      if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
      MainForm.AutomationLogMemoBox.Lines.Add(DateToStr(Now) + ' ' + TimeToStr(Now, fs) + ': ' + 'Sending ACK to Crispin');
    end;
  end;
  //Reset packet
  //CrispinComPort.FlushInBuffer;
  //CrispinComPort.FlushOutBuffer;
  //CrispinComPortPacket.Enabled := FALSE;
  //CrispinComPortPacket.Enabled := TRUE;
end;

//Packet timeout - disable, re-enable
procedure TCrispinInterface.CrispinCOMPortPacketTimeout(Sender: TObject);
begin
  //Log command to GUI memo box
  if (MainForm.AutomationLogMemoBox.Lines.Count > 1000) then MainForm.AutomationLogMemoBox.Lines.Clear;
  MainForm.AutomationLogMemoBox.Lines.Add(DateTimeToStr(Now) + ': Packet timeout occurred - resetting');
  //Reset packet
  CrispinComPortPacket.AutoEnable := FALSE;
  CrispinComPortPacket.Enabled := FALSE;
  CrispinComPortPacket.Enabled := TRUE;
  CrispinComPortPacket.AutoEnable := TRUE;
end;

end.
