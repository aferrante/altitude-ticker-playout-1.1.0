object CrispinInterface: TCrispinInterface
  Left = 308
  Top = 124
  Width = 139
  Height = 161
  Caption = 'CrispinInterface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object CrispinCOMPort: TApdComPort
    AutoOpen = False
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnPortClose = CrispinComPortPortClose
    OnPortOpen = CrispinComPortPortOpen
    Left = 48
    Top = 16
  end
  object CrispinCOMPortPacket: TApdDataPacket
    Enabled = True
    EndCond = [ecString]
    StartString = 'T\'
    EndString = #13#10
    ComPort = CrispinCOMPort
    PacketSize = 0
    OnStringPacket = CrispinCOMPortPacketStringPacket
    OnTimeout = CrispinCOMPortPacketTimeout
    Left = 48
    Top = 64
  end
end
