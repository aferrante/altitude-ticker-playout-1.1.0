unit PlaylistSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids_ts, TSGrid, TSDBGrid, ExtCtrls, DB, SUIButton,
  SUIForm;

type
  TPlaylistSelectDlg = class(TForm)
    Panel1: TPanel;
    AvailablePlaylistGrid: TtsDBGrid;
    Label1: TLabel;
    BitBtn2: TButton;
    BitBtn1: TButton;
    BitBtn3: TButton;
    procedure AvailablePlaylistGridDblClick(Sender: TObject);
    procedure AvailablePlaylistGridRowChanged(Sender: TObject; OldRow,
      NewRow: Variant);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    PlaylistType: SmallInt;
    SelectedPlaylistID: Double;
  end;

var
  PlaylistSelectDlg: TPlaylistSelectDlg;
  SearchModeIsSlug: Boolean;

implementation

uses DataModule, Main;

{$R *.DFM}

//Handler to select entry from active playlist grid (sends message to close dialog)
procedure TPlaylistSelectDlg.AvailablePlaylistGridDblClick(Sender: TObject);
begin
//Same as clicking load active playlist button
  ModalResult := mrOk;
end;

procedure TPlaylistSelectDlg.AvailablePlaylistGridRowChanged(Sender: TObject;
  OldRow, NewRow: Variant);
begin
  //Change the selected playlist ID
  Case PlaylistType of
   1: SelectedPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
//   2: SelectedPlaylistID := dmMain.tblBug_Groups.FieldByName('Playlist_ID').AsFloat;
//   3: SelectedPlaylistID := dmMain.tblExtraLine_Groups.FieldByName('Playlist_ID').AsFloat;
  end;
end;

//Handler to set initial selected playlist ID
procedure TPlaylistSelectDlg.FormActivate(Sender: TObject);
begin
  //Change the data source fo rthe grid and the initial selected playlist ID
  Case PlaylistType of
   1: begin
        //Refresh ticker tables
        dmMain.tblTicker_Groups.Active := FALSE;
        dmMain.tblTicker_Groups.Active := TRUE;
        dmMain.tblTicker_Elements.Active := FALSE;
        dmMain.tblTicker_Elements.Active := TRUE;
        AvailablePlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        SelectedPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
      end;
   2: begin
         //Refresh bug tables
//        dmMain.tblBug_Groups.Active := FALSE;
//        dmMain.tblBug_Groups.Active := TRUE;
//        dmMain.tblBug_Elements.Active := FALSE;
//        dmMain.tblBug_Elements.Active := TRUE;
//        AvailablePlaylistGrid.DataSource := dmMain.dsBug_Groups;
//        SelectedPlaylistID := dmMain.tblBug_Groups.FieldByName('Playlist_ID').AsFloat;
      end;
  end;
end;

end.
