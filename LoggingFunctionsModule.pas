// Rev: 05/04/12  M. Dilworth  Video Design Software Inc.
unit LoggingFunctionsModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ExtCtrls, Globals, FileCtrl;

type
  TLoggingFunctions = class(TForm)
    procedure WriteToCommandLog (CommandString: String);
    procedure WriteToErrorLog (ErrorString: String);
    //Procedure for general data logging - short message only
    procedure WriteToDataLog(Mode: SmallInt; LogStr: String);
    //For posting error log entry to SQL server
    procedure WriteToSQLLog(ChannelID: SmallInt; ErrorCode: LongInt; SupplementalData: String);
    procedure WriteToSQLGPILog(ChannelID: SmallInt; LogEntry: String);
    procedure WriteToSQLAsRunLog(ChannelID: SmallInt; LogEntry: String);
  end;

var
  LoggingFunctions: TLoggingFunctions;

implementation

uses Main, DataModule, OutgoingEMailModule; //Main form


{$R *.DFM}

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//For posting GPI log entry to SQL server
procedure TLoggingFunctions.WriteToSQLGPILog(ChannelID: SmallInt; LogEntry: String);
begin
  try
    if (SQLGPILoggingEnabled) then
    begin
      //Call stored procedure to post data to SQL DB
      with dmMain.LogQuery do
      begin
        Close;
        SQL.Clear;
        SQL.Add('sp_PostGPILogEntry ' +
          IntToStr(ChannelID) + ', ' +
          QuotedStr(LogEntry) + ', ' +
          QuotedStr(DateTimeToStr(Now)));
        ExecSQL;
      end;
      //Call stored procedure to purge log if enabled
      if (SQLGPILogAutoPurgeEnabled) then
      begin
        //Call stored procedure to post data to SQL DB
        with dmMain.LogQuery do
        begin
          Close;
          SQL.Clear;
          SQL.Add('sp_ClearGPILogEntries ' +
            IntToStr(ChannelID) + ', ' +
            QuotedStr(DateTimeToStr(Now-KeepLogEntryDays)));
          ExecSQL;
        end;
      end;
    end;
  except
    //If problem with SQL, write to text log
    LoggingFunctions.WriteToSQLLog(StationIDForLogs, 116, '');
    WriteToErrorLog('General error encountered while trying to post data to SQL GPI Log');
  end;
end;

//For posting As-Run log entry to SQL server
procedure TLoggingFunctions.WriteToSQLAsRunLog(ChannelID: SmallInt; LogEntry: String);
begin
  try
    if (SQLAsRunLoggingEnabled) then
    begin
      //Call stored procedure to post data to SQL DB
      with dmMain.LogQuery do
      begin
        Close;
        SQL.Clear;
        SQL.Add('sp_PostAsRunLogEntry ' +
          IntToStr(ChannelID) + ', ' +
          QuotedStr(LogEntry) + ', ' +
          QuotedStr(DateTimeToStr(Now)));
        ExecSQL;
      end;
      //Call stored procedure to purge log if enabled
      if (SQLAsRunLogAutoPurgeEnabled) then
      begin
        //Call stored procedure to post data to SQL DB
        with dmMain.LogQuery do
        begin
          Close;
          SQL.Clear;
          SQL.Add('sp_ClearAsRunLogEntries ' +
            IntToStr(ChannelID) + ', ' +
            QuotedStr(DateTimeToStr(Now-KeepLogEntryDays)));
          ExecSQL;
        end;
      end;
    end;
  except
    //If problem with SQL, write to text log
    LoggingFunctions.WriteToSQLLog(StationIDForLogs, 118, '');
    WriteToErrorLog('General error encountered while trying to post data to SQL As-Run Log');
  end;
end;

//For posting error log entry to SQL server
procedure TLoggingFunctions.WriteToSQLLog(ChannelID: SmallInt; ErrorCode: LongInt; SupplementalData: String);
var
  i: SmallInt;
  ErrorCodeRecPtr: ^ErrorCodeRec;
  ErrorCodeDescription: String;
  SeverityLevel: SmallInt;
  EMailSubjectText: String;
  EMailMessageBody: String;
begin
  try
    if (SQLErrorLoggingEnabled) then
    begin
      //Call stored procedure to post error to SQL DB
      with dmMain.LogQuery do
      begin
        Close;
        SQL.Clear;
        SQL.Add('sp_PostErrorLogEntry ' +
          IntToStr(ChannelID) + ', ' +
          IntToStr(ErrorCode) + ', ' +
          QuotedStr(DateTimeToStr(Now)) + ', ' +
          QuotedStr(Trim(SupplementalData))
          );
        ExecSQL;
      end;
      //Call stored procedure to purge log if enabled
      if (SQLErrorLogAutoPurgeEnabled) then
      begin
        //Call stored procedure to post data to SQL DB
        with dmMain.LogQuery do
        begin
          Close;
          SQL.Clear;
          SQL.Add('sp_ClearErrorLogEntries ' +
            IntToStr(ChannelID) + ', ' +
            QuotedStr(DateTimeToStr(Now-KeepLogEntryDays)));
          ExecSQL;
        end;
      end;
    end;
  except
    //If problem with SQL, write to text log
    WriteToErrorLog('General error encountered while trying to post error to SQL Log');
  end;

  //If it's a severity level one error, send out the e-mail alert
  //Also, update the scrollbar & set the master alarm GPO
  if (StartupComplete) then
  try
    if (Error_Codes_Collection.Count > 0) then
    begin
      //Lookup the severity level
      for i := 0 to Error_Codes_Collection.Count-1 do
      begin
        ErrorCodeRecPtr := Error_Codes_Collection.At(i);
        if (ErrorCodeRecPtr^.ErrorCode = ErrorCode) then
        begin
          SeverityLevel := ErrorCodeRecPtr^.SeverityLevel;
          ErrorCodeDescription := ErrorCodeRecPtr^.LogEntry;
        end;
      end;
    end;

    //Handle P1 error condition
    if (SeverityLevel = SEVERITY_LEVEL_ONE) then
    begin
      //Light the Alarm LED
      MainForm.AlarmConditionLED.Lit := TRUE;
      //SystemStatus[MASTER_ALARM_STATUS] := 1;

      //Update status bar
      MainForm.Statusbar.Color := clRed;
      MainForm.Statusbar.SimpleText := 'SERVERITY LEVEL 1 ERROR: ' + IntToStr(ErrorCode) + ' ' +
        ErrorCodeDescription;

      //If it's severity level one and we're past the throttle interval, send the e-mail
      if ((Now - LastP1AlertEMailTimestamp) > (P1AlertEMailMinTimeInterval/(24*60))) then
      begin
        EMailSubjectText := 'Altitude Sports Ticker Severity 1 Alert ' + DateTimeToStr(Now);
        EMailMessageBody :=  'ApplicationName: ' + StationDescription + #13#10 +
          'Playout Station ID: ' + IntToStr(ChannelID) + #13#10 +
          'Error Code: ' + IntToStr(ErrorCode) + #13#10 +
          'Error Description: ' + ErrorCodeDescription + ' ' + SupplementalData + #13#10;
        OutgoingEMail.SendEMail (EMail_From_Address, EMail_To_Addresses, EMailSubjectText, EMailMessageBody);
        //Set new timestamp for last e-mail sent
        LastP1AlertEMailTimestamp := Now;
      end;
    end
    //Level 2 error
    else if (SeverityLevel = SEVERITY_LEVEL_TWO) then
    begin
      //Update status bar
      MainForm.Statusbar.Color := clYellow;
      MainForm.Statusbar.SimpleText := 'SERVERITY LEVEL 2 ERROR: ' + IntToStr(ErrorCode) + ' ' +
        ErrorCodeDescription;
    end
    //Level 3 error - just log it - no color/fault indication
    else if (SeverityLevel = SEVERITY_LEVEL_THREE) then
    begin
      //Update status bar
      MainForm.Statusbar.Color := clSilver;
      MainForm.Statusbar.SimpleText := 'SERVERITY LEVEL 3 ERROR: ' + IntToStr(ErrorCode) + ' ' +
        ErrorCodeDescription;
    end;
  except

  end;
end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TLoggingFunctions.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   //Check for error log directory and create if it doesn't exist
   if (DirectoryExists('c:\Altitude_Error_LogFiles') = FALSE) then CreateDir('c:\Altitude_Error_LogFiles');
   //Build date and time strings
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '  ' + DateStr + '  ' + ErrorString;

   //Check for error log directory and create if it doesn't exist
   if (DirectoryExists(ErrorLogFileDirectoryPath) = FALSE) then
   try
     if (DirectoryExists('c:\Altitude_Error_LogFiles') = FALSE) then
       CreateDir('c:\Altitude_Error_LogFiles');
     DirectoryStr := 'c:\Altitude_Error_LogFiles\';
   except
     DirectoryStr := 'c:\Altitude_Error_LogFiles\';
   end
   else DirectoryStr := ErrorLogFileDirectoryPath;

   //Construct filename using current date - files are numbered 01 through 31
   //Construct filename using current date - files are numbered 01 through 31
   FileName := DirectoryStr + 'AltitudeErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);

   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);
      finally
         CloseFile (ErrorLogFile);
      end;
   end
   else begin
      //Check file to see if it's a month old. If so, erase and create a new one
      Reset (ErrorLogFile);
      try
         Readln (ErrorLogFile, TestStr);
      finally
         CloseFile (ErrorLogFile);
      end;
      If ((TestStr[11] <> DayStr[1]) OR (TestStr[12] <> DayStr[2])) then
      begin
         //Date in 1st entry in error log does not match today's date, so erase
         Erase (ErrorLogFile);
         //Create a new logfile}
         ReWrite (ErrorLogFile);
         try
            Writeln (ErrorLogFile, ErrorLogStr);
         finally
            CloseFile (ErrorLogFile);
         end;
      end
      else begin
         //File exists and not a month old, so append to existing log file
         Append (ErrorLogFile);
         try
            Writeln (ErrorLogFile, ErrorLogStr);
         finally
            CloseFile (ErrorLogFile);
         end;
      end;
   end;
end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TLoggingFunctions.WriteToCommandLog (CommandString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   CommandLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   //Check for error log directory and create if it doesn't exist
   if (DirectoryExists(CommandLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Altitude_Command_LogFiles') = FALSE) then
       CreateDir('c:\Altitude_Command_LogFiles');
     DirectoryStr := 'c:\Altitude_Command_LogFiles\';
   end
   else DirectoryStr := CommandLogFileDirectoryPath;
   //Build date and time strings
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   CommandLogStr := TimeStr + '  ' + DateStr + '  ' + CommandString +
     '[TIME = ' + IntToStr(Hour) + ':' + IntToStr(Min) + ':' + IntToStr(Sec) +
     ':' + IntToStr(MSec) + ']';
   //Construct filename using current date - files are numbered 01 through 31
   FileName := DirectoryStr + '\' + 'AltitudeCommandLog' + DayStr + '.txt';
   AssignFile (CommandLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (CommandLogFile);
      try
         Writeln (CommandLogFile, CommandLogStr);
      finally
         CloseFile (CommandLogFile);
      end;
   end
   else begin
      //Check file to see if it's a month old by comparing the months. If so, erase and create a new one
      Reset (CommandLogFile);
      try
         Readln (CommandLogFile, TestStr);
      finally
         CloseFile (CommandLogFile);
      end;
      If ((TestStr[11] <> MonthStr[1]) OR (TestStr[12] <> MonthStr[2])) then
      begin
         //Date in 1st entry in error log does not match today's date, so erase
         Erase (CommandLogFile);
         //Create a new logfile
         ReWrite (CommandLogFile);
         try
            Writeln (CommandLogFile, CommandLogStr);
         finally
            CloseFile (CommandLogFile);
         end;
      end
      else begin
         //File exists and not a month old, so append to existing log file
         Append (CommandLogFile);
         try
            Writeln (CommandLogFile, CommandLogStr);
         finally
            CloseFile (CommandLogFile);
         end;
      end;
   end;
end;

//Historical data logging function
procedure TLoggingFunctions.WriteToDataLog(Mode: SmallInt; LogStr: String);
var
   i: SmallInt;
   FileNameStr: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   DataLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   MyTickerSymbol1_Str: String;
   MyTickerSymbol2_Str: String;
   MyGraphDuration: SmallInt;
   MyGraphType: SmallInt;
   GraphDurationStr: String;
   GraphCountStr: String;
   GraphCount: SmallInt;
   InStr: String;
   DirectoryStr: String;
begin
   //Check for error log directory and create if it doesn't exist
   if (DirectoryExists(DataLogFileDirectoryPath) = FALSE) then
   try
     if (DirectoryExists('c:\Altitude_Data_LogFiles') = FALSE) then
       CreateDir('c:\Altitude_Data_LogFiles');
     DirectoryStr := 'c:\Altitude_Data_LogFiles\';
   except
     DirectoryStr := 'c:\Altitude_Data_LogFiles\';
   end
   else DirectoryStr := DataLogFileDirectoryPath;

   //Build date and time strings
   DecodeDate (Now, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);

   DatalogStr := FormatDateTime('hh:nn:ss:zzz  mm-dd-yyyy',now);
   //Construct filename using current date - files are numbered 01 through 31
   FileNameStr := DirectoryStr + '\DataLog' + DayStr + '.txt';
   AssignFile (DataLogFile, FileNameStr);

   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileNameStr))) then
   begin
      ReWrite (DataLogFile); //Create new file
      try
         if (Mode = GENERIC_MESSAGE) then
         begin
           DataLogStr := DataLogStr + '  ' + LogStr;
           Writeln (DataLogFile, DataLogStr);
         end;
      finally
         CloseFile (DataLogFile);
      end;
    end
    else begin
      //Check file to see if it's a month old. If so, erase and create a new one
      Reset (DataLogFile);
      try
        Readln (DataLogFile, TestStr);
      finally
        CloseFile (DataLogFile);
      end;
      if (Length(TestStr) >= 16) AND ((TestStr[15] <> MonthStr[1]) OR (TestStr[16] <> MonthStr[2])) then
      begin
        //Month in 1st entry in error log does not match today's month, so erase
        Erase (DataLogFile);
        //Create a new logfile
        ReWrite (DataLogFile);
        try
          if (Mode = GENERIC_MESSAGE) then
          begin
            DataLogStr := DataLogStr + '  ' + LogStr;
            Writeln (DataLogFile, DataLogStr);
          end;
         finally
            CloseFile (DataLogFile);
         end;
       end
       else begin
         //File exists and not a month old, so append to existing log file
         //First, update graph count string
         Reset (DataLogFile);
         try
           while not SeekEof(DataLogFile) do
             Readln (DataLogFile, InStr);
         finally
           CloseFile (DataLogFile);
         end;
         //Now, append log info to existing log file
         Append (DataLogFile);
         try
            if (Mode = GENERIC_MESSAGE) then
            begin
              DataLogStr := DataLogStr + '  ' + LogStr;
              Writeln (DataLogFile, DataLogStr);
            end;
         finally
            CloseFile (DataLogFile);
         end;
      end;
   end;
end;

end.


