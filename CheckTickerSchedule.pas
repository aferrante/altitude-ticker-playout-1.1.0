//This thread class checks for a scheduled ticker playlist
unit CheckTickerSchedule;

interface

uses
  Classes;

type
  TCheckTickerSchedule = class(TThread)
  private
    { Private declarations }
  protected
    procedure Execute; override;
    procedure UpdateMainTickerPlaylist;
    procedure RefreshCurrentMainTickerPlaylist;
    procedure UpdateBreakingNewsPlaylist;
    procedure WriteToErrorLogMainTicker;
    procedure WriteToErrorLogBug;
    procedure WriteToErrorLogExtraLine;
    procedure ClearBreakingNewsCollection;
    procedure SetGridDataRow;
  end;

implementation

uses Globals,
     DataModule,
     SysUtils,
     Main,
     EngineIntf,
     LoggingFunctionsModule;

  { TCheckTickerSchedule }

//MAIN TICKER
procedure TCheckTickerSchedule.SetGridDataRow;
begin
  MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
  MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
end;

//Grid, labels and playlist collection objects updated via call to synchronize method
procedure TCheckTickerSchedule.UpdateMainTickerPlaylist;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  LogStr: String;
begin
  //Set Checking ticker playlist flag
  UpdatingTickerPlaylist := TRUE;

  //Load the collection; set flag to indicate it's the first time through
  MainForm.LoadPlaylistCollection(TICKER, 0, ScheduledPlaylistInfo[1].Playlist_ID);

  //Refresh the status labels
  MainForm.TickerPlaylistName.Caption := ScheduledPlaylistInfo[1].Playlist_Description;
  MainForm.TickerStartTimeLabel.Caption := DateTimeToStr(ScheduledPlaylistInfo[1].Start_Enable_Time);
  MainForm.TickerEndTimeLabel.Caption := DateTimeToStr(ScheduledPlaylistInfo[1].End_Enable_Time);

  //Added logging V3.0.14
  EngineInterface.WriteToAsRunLog('Ticker playlist automatically loaded: ' + ScheduledPlaylistInfo[1].Playlist_Description +
    ' (Playlist ID ' + FloatToStr(ScheduledPlaylistInfo[1].Playlist_ID) + ')');

  //Set the ticker display mode
  TickerDisplayMode := ScheduledPlaylistInfo[1].Ticker_Mode;

  //Added logging V3.0.14
  EngineInterface.WriteToAsRunLog('Ticker display mode for Playlist ID ' + FloatToStr(ScheduledPlaylistInfo[1].Playlist_ID) + ': ' + IntToStr(TickerDisplayMode));
  EngineInterface.WriteToAsRunLog('Start Enable Time for Playlist ID ' + FloatToStr(ScheduledPlaylistInfo[1].Playlist_ID) + ': ' + DateTimeToStr(ScheduledPlaylistInfo[1].Start_Enable_Time));
  EngineInterface.WriteToAsRunLog('End Enable Time for Playlist ID ' + FloatToStr(ScheduledPlaylistInfo[1].Playlist_ID) + ': ' + DateTimeToStr(ScheduledPlaylistInfo[1].End_Enable_Time));

  //Added logging V3.0.14
  //Log first entry in playlist
  if (Ticker_Collection.Count > 0) then
  begin
    TickerRecPtr := Ticker_Collection.At(0);
    LogStr := TickerRecPtr^.League + ' ';
    if (TickerRecPtr^.Record_Type = 1) OR (TickerRecPtr^.Record_Type = 37) OR (TickerRecPtr^.Record_Type = 45) then
      LogStr := LogStr + TickerRecPtr^.SponsorLogo_Name
    else if (TickerRecPtr^.Record_Type = 2) OR (TickerRecPtr^.Record_Type = 3) then
      LogStr := LogStr + TickerRecPtr^.UserData[1]
    else if (MainForm.GetTemplateInformation(TickerRecPtr^.Template_ID).UsesGameData = TRUE) then
      LogStr := LogStr + TickerRecPtr^.Description
    else
      LogStr := LogStr + TickerRecPtr^.UserData[1];
    //Set commends
    LogStr := LogStr + ' ' + TickerRecPtr^.Comments; //Record enable
    EngineInterface.WriteToAsRunLog('First Ticker Record Data for Playlist ID ' + FloatToStr(ScheduledPlaylistInfo[1].Playlist_ID) + ': ' + LogStr);
  end;

  Case TickerDisplayMode of
    1: begin
         MainForm.OneTimeLoopModeLabel.Visible := TRUE;
         MainForm.TickerMode.Caption := '1-LINE MODE';
       end;
    2: begin
         MainForm.OneTimeLoopModeLabel.Visible := FALSE;
         MainForm.TickerMode.Caption := '1-LINE MODE';
       end;
    3: begin
         MainForm.OneTimeLoopModeLabel.Visible := TRUE;
         MainForm.TickerMode.Caption := '2-LINE MODE';
       end;
    4: begin
         MainForm.OneTimeLoopModeLabel.Visible := FALSE;
         MainForm.TickerMode.Caption := '2-LINE MODE';
       end;
  end;

  //Set the entry index to the first entry
  CurrentTickerEntryIndex := 0;

  //Update the grid
  MainForm.RefreshPlaylistGrid(1);

  //Set the current playlist ID - allows for refresh of collection of more than one iteration
  CurrentTickerPlaylistID := ScheduledPlaylistInfo[1].Playlist_ID;
  CurrentTickerPlaylistStartTime := ScheduledPlaylistInfo[1].Start_Enable_Time;

  //Clear the sponsor logo information
  //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
  CurrentSponsorInfo.CurrentSponsorLogoName := '';
  CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
  CurrentSponsorInfo.CurrentSponsorTemplate := 0;
  CurrentSponsorInfo.Tagline_Top := '';
  CurrentSponsorInfo.Tagline_Bottom := '';

  //Clear Checking ticker playlist flag
  UpdatingTickerPlaylist := FALSE;
end;

//This function will refresh the current playlist; leaves sponsor logo info intact
//Grid, labels and playlist collection objects updated via call to synchronize method
procedure TCheckTickerSchedule.RefreshCurrentMainTickerPlaylist;
var
  i: SmallInt;
begin
  //Set Checking ticker playlist flag
  UpdatingTickerPlaylist := TRUE;

  //Load the collection; set flag to indicate it's the first time through
  MainForm.LoadPlaylistCollection(TICKER, 0, ScheduledPlaylistInfo[1].Playlist_ID);

  //Refresh the status labels
  MainForm.TickerPlaylistName.Caption := ScheduledPlaylistInfo[1].Playlist_Description;
  MainForm.TickerStartTimeLabel.Caption := DateTimeToStr(ScheduledPlaylistInfo[1].Start_Enable_Time);
  MainForm.TickerEndTimeLabel.Caption := DateTimeToStr(ScheduledPlaylistInfo[1].End_Enable_Time);

  //Set the ticker display mode
  TickerDisplayMode := ScheduledPlaylistInfo[1].Ticker_Mode;
  Case TickerDisplayMode of
    1: begin
         MainForm.OneTimeLoopModeLabel.Visible := TRUE;
         MainForm.TickerMode.Caption := '1-LINE MODE';
       end;
    2: begin
         MainForm.OneTimeLoopModeLabel.Visible := FALSE;
         MainForm.TickerMode.Caption := '1-LINE MODE';
       end;
    3: begin
         MainForm.OneTimeLoopModeLabel.Visible := TRUE;
         MainForm.TickerMode.Caption := '2-LINE MODE';
       end;
    4: begin
         MainForm.OneTimeLoopModeLabel.Visible := FALSE;
         MainForm.TickerMode.Caption := '2-LINE MODE';
       end;
  end;

  //Set the entry index to the first entry
  CurrentTickerEntryIndex := 0;

  //Update the grid
  MainForm.RefreshPlaylistGrid(1);

  //Set the current playlist ID - allows for refresh of collection of more than one iteration
  CurrentTickerPlaylistID := ScheduledPlaylistInfo[1].Playlist_ID;
  CurrentTickerPlaylistStartTime := ScheduledPlaylistInfo[1].Start_Enable_Time;

  //Clear Checking ticker playlist flag
  UpdatingTickerPlaylist := FALSE;
end;

//BREAKING NEWS
//Grid, labels and playlist collection objects updated via call to synchronize method
procedure TCheckTickerSchedule.UpdateBreakingNewsPlaylist;
begin
  if (MasterSegmentIndex <> 2) then
  begin
    //Set Checking ticker playlist flag
    UpdatingBreakingNewsPlaylist := TRUE;

    if (BreakingNewsSettings.BreakingNewsEnable) then
    begin
      //Load the collection; set flag to indicate it's the first time through
      MainForm.LoadPlaylistCollection(BREAKINGNEWS, 0, ScheduledBreakingNewsPlaylistID);
    end
    else begin
      BreakingNews_Collection.Clear;
      BreakingNews_Collection.Pack;
      MainForm.NumBreakingNewsEntries.Caption := '0';
    end;

    //Set the entry index to the first entry
    CurrentBreakingNewsEntryIndex := 0;

    //Update the grid
    MainForm.RefreshPlaylistGrid(2);

    //Set the current playlist ID - allows for refresh of collection of more than one iteration
    CurrentBreakingNewsPlaylistID := 0;

    //Clear Checking ticker playlist flag
    UpdatingBreakingNewsPlaylist := FALSE;
  end;
end;

//Procedure to clear the extra line collection if no playlists are scheduled
procedure TCheckTickerSchedule.ClearBreakingNewsCollection;
begin
  //Clear the collection
  if (BreakingNews_Collection.Count > 0) then
  begin
    BreakingNews_Collection.Clear;
    BreakingNews_Collection.Pack;

    MainForm.NumBreakingNewsEntries.Caption := '0';

    //Set the entry index; will be incremented to the first entry
    CurrentBreakingNewsEntryIndex := -1;

    //Set the current playlist ID - allows for refresh of collection of more than one iteration
    CurrentBreakingNewsPlaylistID := -1;
    CurrentBreakingNewsPlaylistStartTime := 0;
    CurrentBreakingNewsPlaylistEndTime := 0;

    //Update the grid
    MainForm.RefreshPlaylistGrid(2);
  end;
end;

//ERROR LOGGING PROCEDURES
//Write to the main ticker error log on exception
procedure TCheckTickerSchedule.WriteToErrorLogMainTicker;
begin
  LoggingFunctions.WriteToSQLLog(StationIDForLogs, 318, '');
  EngineInterface.WriteToErrorLog('Error encountered while trying to locate scheduled main ticker playlist.');
end;

//Write to the bug error log on exception
procedure TCheckTickerSchedule.WriteToErrorLogBug;
begin
  EngineInterface.WriteToErrorLog('Error encountered while trying to locate scheduled bug playlist.');
end;

//Write to the extra line error log on exception
procedure TCheckTickerSchedule.WriteToErrorLogExtraLine;
begin
  EngineInterface.WriteToErrorLog('Error encountered while trying to locate scheduled extra line playlist.');
end;

//Main thread execution method
procedure TCheckTickerSchedule.Execute;
var
  FoundScheduledGroup: Boolean;
  FoundBreakingNewsPlaylist: Boolean;
  CurrentTime: TDateTime;
  OKToClear: Boolean;
  QueryStr: String;
  CurrentMode, CheckMode: SmallInt;
  SavePos: SmallInt;
begin
  if (NOT(Terminated)) then
  begin
    //Ticker PLAYLIST
    if (RunningThread = FALSE) then
    begin
      //Set flag
      RunningThread := TRUE;
      //Init
      FoundScheduledGroup := FALSE;
      OKToClear := TRUE;
      //Refresh tables
      try
        if (UpdatingTickerPlaylist = FALSE) AND (LoadingPlaylist = FALSE) then
        begin
          dmMain.Query4.Active := FALSE;
          dmMain.Query4.SQL.Clear;
          //Filter for global playlists or playlists specific to this station ID
          dmMain.Query4.SQL.Add('SELECT * FROM Scheduled_Ticker_Groups WHERE Station_ID = ' +
            IntToStr(0) + ' OR Station_ID = ' + IntToStr(StationID));
          dmMain.Query4.Active := TRUE;
          //Search the database for the first occurrence of a start time > now
          if (dmMain.Query4.RecordCount > 0) then
          begin
            try
              //Only look for new scheduled playlist if schedule monitoring enabled
              if (ScheduleMonitoringEnabled = TRUE) then
              begin
                //Walk through database to find scheduled playlist that has not expired
                CurrentTime := NOW;
                Repeat
                  //If matching playlist, update values & reload playlist
                  if (CurrentTickerPlaylistID = dmMain.Query4.FieldByName('Playlist_ID').AsFloat) then
                  begin
                    ScheduledPlaylistInfo[1].Start_Enable_Time := dmMain.Query4.FieldByName('Start_Enable_Time').AsDateTime;
                    ScheduledPlaylistInfo[1].End_Enable_Time := dmMain.Query4.FieldByName('End_Enable_Time').AsDateTime;
                    ScheduledPlaylistInfo[1].Ticker_Mode := dmMain.Query4.FieldByName('Ticker_Mode').AsInteger;
                    ScheduledPlaylistInfo[1].Station_ID := dmMain.Query4.FieldByName('Station_ID').AsInteger;
                    CurrentTickerPlaylistStartTime := ScheduledPlaylistInfo[1].Start_Enable_Time;

                    //If not running ticker, refresh the current playlist
                    //Preserve current ticker position so it can be restored after check for new playlist
                    SavePos := CurrentTickerEntryIndex;
                    if (RunningTicker = FALSE) then Synchronize(RefreshCurrentMainTickerPlaylist);

                    //Restore current position in playlist & grid
                    if (CurrentTickerEntryIndex < Ticker_Collection.Count) then
                      CurrentTickerEntryIndex := SavePos
                    else
                      CurrentTickerEntryIndex := 0;
                    //Modified V3.0.12 to make thread safe
                    Synchronize(SetGridDataRow);
                  end;

                  CurrentMode := ScheduledPlaylistInfo[1].Ticker_Mode;
                  CheckMode := dmMain.Query4.FieldByName('Ticker_Mode').AsInteger;

                  //Do check for new playlist if different mode (1-line vs. 2-line)
                  if (dmMain.Query4.FieldByName('Start_Enable_Time').AsDateTime <= CurrentTime) AND
                     (dmMain.Query4.FieldByName('End_Enable_Time').AsDateTime >= CurrentTime) AND
                     ((CurrentTickerPlaylistID <> dmMain.Query4.FieldByName('Playlist_ID').AsFloat)
                     AND
                     //Added check for loading new playlist with newer start time
                     //Added check to also switch if the start times are identical and there is only one record
                     //(could mean that that current schedule entry has been saved over by a newer one)
                     (((CurrentTime - dmMain.Query4.FieldByName('Start_Enable_Time').AsDateTime) <
                      (CurrentTime - CurrentTickerPlaylistStartTime)) OR (dmMain.Query4.RecordCount=1)))
                      AND
                     //Added check to prevent attempt to live switch from 1-line to 2-line or vice-versa
                     ((((((CurrentMode = 1) OR (CurrentMode = 2)) AND ((CheckMode = 1) OR (CheckMode = 2))) OR
                     (((CurrentMode = 3) OR (CurrentMode = 4)) AND ((CheckMode = 3) OR (CheckMode = 4)))) AND (RunningTicker = TRUE)) OR
                     (RunningTicker = FALSE)) then
                  begin
                    //Set flag
                    FoundScheduledGroup := TRUE;
                    //Set values
                    ScheduledPlaylistInfo[1].Playlist_ID := dmMain.Query4.FieldByName('Playlist_ID').AsFloat;
                    ScheduledPlaylistInfo[1].Playlist_Description := dmMain.Query4.FieldByName('Playlist_Description').AsString;
                    ScheduledPlaylistInfo[1].Start_Enable_Time := dmMain.Query4.FieldByName('Start_Enable_Time').AsDateTime;
                    ScheduledPlaylistInfo[1].End_Enable_Time := dmMain.Query4.FieldByName('End_Enable_Time').AsDateTime;
                    ScheduledPlaylistInfo[1].Ticker_Mode := dmMain.Query4.FieldByName('Ticker_Mode').AsInteger;
                    ScheduledPlaylistInfo[1].Station_ID := dmMain.Query4.FieldByName('Station_ID').AsInteger;
                    //A match was found, so call the method to update the collection and go from there
                    Synchronize(UpdateMainTickerPlaylist);
                  end;
                  //Go to next record
                  dmMain.Query4.Next;
                Until (FoundScheduledGroup = TRUE) OR (dmMain.Query4.EOF = TRUE);
              end;
            except
              //Call method to write to the error log
              Synchronize(WriteToErrorLogMainTicker);
              //Clear flag
              RunningThread := FALSE;
            end;
          end;
        end;
      except
        //Call method to write to the error log
        Synchronize(WriteToErrorLogMainTicker);
        //Clear flag
        RunningThread := FALSE;
      end;
      //Clear flag
      RunningThread := FALSE;
    end;

    //BREAKING NEWS PLAYLIST
    if (RunningThread = FALSE) then
    begin
      try
        //Refresh tables
        if (UpdatingBreakingNewsPlaylist = FALSE) AND (LoadingPlaylist = FALSE) then
        begin
          //Always check for Breaking News
          dmMain.Query5.Close;
          dmMain.Query5.SQL.Clear;
          //Filter for global playlists or playlists specific to this station ID
          dmMain.Query5.SQL.Add('SELECT * FROM BreakingNews_Groups WHERE Playlist_ID = ' +
            IntToStr(0) + ' OR Playlist_ID = ' + IntToStr(StationID));
          dmMain.Query5.Open;
          if (dmMain.Query5.RecordCount > 0) then
          begin
            //Set breaking news playlist ID
            ScheduledBreakingNewsPlaylistID := dmMain.Query5.FieldByName('Playlist_ID').AsFloat;
            BreakingNewsSettings.BreakingNewsEnable :=
              dmMain.Query5.FieldByName('BreakingNews_Enable').AsBoolean;
            //Reset if breaking news is now disabled
            if (BreakingNewsSettings.BreakingNewsEnable = FALSE) then
              BreakingNewsIterationsCompleted := 0;
            //Reset iterations if switching to immediate mode
            if (BreakingNewsSettings.BreakingNewsMode = 1) AND
               (dmMain.Query5.FieldByName('BreakingNews_Mode').AsInteger = 0) then
              BreakingNewsIterationsCompleted := 0;
            BreakingNewsSettings.BreakingNewsMode :=
              dmMain.Query5.FieldByName('BreakingNews_Mode').AsInteger;
            BreakingNewsSettings.Iterations :=
              dmMain.Query5.FieldByName('Iterations').AsInteger;
          end;
          dmMain.Query5.Close;
          //Set flags
          LoadingPlaylist := TRUE;
          UpdatingBreakingNewsPlaylist := TRUE;
          Synchronize(UpdateBreakingNewsPlaylist);
        end;
      except
        //Call method to write to the error log
        Synchronize(WriteToErrorLogBug);
        //Clear flag
        RunningThread := FALSE;
      end;
      //Clear flag
      RunningThread := FALSE;
      UpdatingBreakingNewsPlaylist := FALSE;
      LoadingPlaylist := FALSE;
    end;
  end;
end;

end.
