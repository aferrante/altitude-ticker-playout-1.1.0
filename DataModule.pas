Unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, {DBISAMTb,} {MSGParserServer,} ScktComp, ExtCtrls;

type
  ERecvError = class(Exception);

  TdmMain = class(TDataModule)
    dbTicker: TADOConnection;
    tblTicker_Groups: TADOTable;
    tblTicker_Elements: TADOTable;
    dsTicker_Groups: TDataSource;
    Query1: TADOQuery;
    dsQuery1: TDataSource;
    tblCustom_Segments: TADOTable;
    tblScheduled_Ticker_Groups: TADOTable;
    dsScheduled_Ticker_Groups: TDataSource;
    tblSponsor_Logos: TADOTable;
    dsSponsor_Logos: TDataSource;
    tblGame_Notes: TADOTable;
    tblLeagues: TADOTable;
    tblPromo_Logos: TADOTable;
    dsPromo_Logos: TDataSource;
    dbSportbase: TADOConnection;
    SportbaseQuery: TADOQuery;
    dsSportbaseQuery: TDataSource;
    Query2: TADOQuery;
    dsQuery2: TDataSource;
    Query3: TADOQuery;
    SportbaseQuery2: TADOQuery;
    tblBreakingNews_Elements: TADOTable;
    tblBreakingNews_Groups: TADOTable;
    dsBreakingNews_Groups: TDataSource;
    SportbaseQuery3: TADOQuery;
    dsSportbaseQuery3: TDataSource;
    tblTemplate_Defs: TADOTable;
    tblTeams: TADOTable;
    dsTeams: TDataSource;
    tblGame_Phase_Codes: TADOTable;
    dsGame_Phase_Codes: TDataSource;
    Query4: TADOQuery;
    GameDataQuery: TADOQuery;
    WeatherQuery: TADOQuery;
    PlaylistQuery: TADOQuery;
    dsPlaylistQuery: TDataSource;
    LogQuery: TADOQuery;
    dbMasterControl: TADOConnection;
    Query5: TADOQuery;
    PlaylistQuery2: TADOQuery;
    tblAsRunLog: TADOTable;
    tblErrorLog: TADOTable;
    dsAsRunLog: TDataSource;
    dsErrorLog: TDataSource;
    dbLogoImages: TADOConnection;
    LogoUpdateQuery: TADOQuery;
  private
  public
  end;

var
  dmMain: TdmMain;

implementation

uses Main;

{$R *.DFM}

end.
