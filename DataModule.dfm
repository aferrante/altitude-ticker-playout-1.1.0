object dmMain: TdmMain
  OldCreateOrder = False
  Left = 478
  Top = 23
  Height = 842
  Width = 883
  object dbTicker: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 47
    Top = 30
  end
  object tblTicker_Groups: TADOTable
    Connection = dbTicker
    IndexFieldNames = 'Playlist_Description'
    TableName = 'Ticker_Groups'
    Left = 135
    Top = 150
  end
  object tblTicker_Elements: TADOTable
    Connection = dbTicker
    TableName = 'Ticker_Elements'
    Left = 135
    Top = 94
  end
  object dsTicker_Groups: TDataSource
    DataSet = tblTicker_Groups
    Left = 295
    Top = 150
  end
  object Query1: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 423
    Top = 35
  end
  object dsQuery1: TDataSource
    DataSet = Query1
    Left = 543
    Top = 33
  end
  object tblCustom_Segments: TADOTable
    Connection = dbTicker
    TableName = 'Custom_Segments'
    Left = 135
    Top = 30
  end
  object tblScheduled_Ticker_Groups: TADOTable
    Connection = dbTicker
    TableName = 'Scheduled_Ticker_Groups'
    Left = 135
    Top = 214
  end
  object dsScheduled_Ticker_Groups: TDataSource
    DataSet = tblScheduled_Ticker_Groups
    Left = 296
    Top = 214
  end
  object tblSponsor_Logos: TADOTable
    Connection = dbTicker
    TableName = 'Sponsor_Logos'
    Left = 425
    Top = 418
  end
  object dsSponsor_Logos: TDataSource
    DataSet = tblSponsor_Logos
    Left = 545
    Top = 419
  end
  object tblGame_Notes: TADOTable
    Connection = dbTicker
    TableName = 'Game_Notes'
    Left = 423
    Top = 351
  end
  object tblLeagues: TADOTable
    Connection = dbTicker
    TableName = 'Leagues'
    Left = 425
    Top = 554
  end
  object tblPromo_Logos: TADOTable
    Connection = dbTicker
    TableName = 'Promo_Logos'
    Left = 425
    Top = 490
  end
  object dsPromo_Logos: TDataSource
    DataSet = tblPromo_Logos
    Left = 545
    Top = 491
  end
  object dbSportbase: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SportBase'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 663
    Top = 35
  end
  object SportbaseQuery: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 663
    Top = 99
  end
  object dsSportbaseQuery: TDataSource
    DataSet = SportbaseQuery
    Left = 783
    Top = 99
  end
  object Query2: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 423
    Top = 91
  end
  object dsQuery2: TDataSource
    DataSet = Query2
    Left = 543
    Top = 89
  end
  object Query3: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 423
    Top = 155
  end
  object SportbaseQuery2: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 663
    Top = 163
  end
  object tblBreakingNews_Elements: TADOTable
    Connection = dbTicker
    TableName = 'BreakingNews_Elements'
    Left = 135
    Top = 278
  end
  object tblBreakingNews_Groups: TADOTable
    Connection = dbTicker
    TableName = 'BreakingNews_Groups'
    Left = 135
    Top = 334
  end
  object dsBreakingNews_Groups: TDataSource
    DataSet = tblBreakingNews_Groups
    Left = 295
    Top = 334
  end
  object SportbaseQuery3: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 663
    Top = 227
  end
  object dsSportbaseQuery3: TDataSource
    DataSet = SportbaseQuery3
    Left = 783
    Top = 227
  end
  object tblTemplate_Defs: TADOTable
    Connection = dbTicker
    TableName = 'Template_Defs'
    Left = 135
    Top = 390
  end
  object tblTeams: TADOTable
    Connection = dbTicker
    TableName = 'Teams'
    Left = 429
    Top = 626
  end
  object dsTeams: TDataSource
    DataSet = tblTeams
    Left = 533
    Top = 627
  end
  object tblGame_Phase_Codes: TADOTable
    Connection = dbTicker
    TableName = 'Game_Phase_Codes'
    Left = 661
    Top = 442
  end
  object dsGame_Phase_Codes: TDataSource
    DataSet = tblGame_Phase_Codes
    Left = 781
    Top = 443
  end
  object Query4: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 423
    Top = 219
  end
  object GameDataQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 423
    Top = 283
  end
  object WeatherQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 543
    Top = 283
  end
  object PlaylistQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 135
    Top = 467
  end
  object dsPlaylistQuery: TDataSource
    DataSet = PlaylistQuery
    Left = 263
    Top = 465
  end
  object LogQuery: TADOQuery
    Connection = dbMasterControl
    Parameters = <>
    Left = 135
    Top = 565
  end
  object dbMasterControl: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 47
    Top = 566
  end
  object Query5: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 503
    Top = 219
  end
  object PlaylistQuery2: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 135
    Top = 515
  end
  object tblAsRunLog: TADOTable
    Connection = dbMasterControl
    TableName = 'AsRunLog'
    Left = 135
    Top = 638
  end
  object tblErrorLog: TADOTable
    Connection = dbMasterControl
    TableName = 'ErrorLog'
    Left = 135
    Top = 694
  end
  object dsAsRunLog: TDataSource
    DataSet = tblAsRunLog
    Left = 215
    Top = 639
  end
  object dsErrorLog: TDataSource
    DataSet = tblErrorLog
    Left = 215
    Top = 695
  end
  object dbLogoImages: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 431
    Top = 710
  end
  object LogoUpdateQuery: TADOQuery
    Connection = dbLogoImages
    Parameters = <>
    Left = 535
    Top = 709
  end
end
