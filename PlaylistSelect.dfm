object PlaylistSelectDlg: TPlaylistSelectDlg
  Left = 666
  Top = 403
  BorderStyle = bsDialog
  Caption = 'Playlist Selection for Load'
  ClientHeight = 331
  ClientWidth = 625
  Color = clBtnFace
  Constraints.MinHeight = 29
  Constraints.MinWidth = 150
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn2: TButton
    Left = 214
    Top = 288
    Width = 198
    Height = 33
    Caption = '&Close'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 593
    Height = 273
    BevelWidth = 2
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 134
      Height = 16
      Caption = 'Available Playlists:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object AvailablePlaylistGrid: TtsDBGrid
      Left = 16
      Top = 35
      Width = 560
      Height = 182
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      ColMoving = False
      Cols = 3
      ColSelectMode = csNone
      DatasetType = dstStandard
      DataSource = dmMain.dsPlaylistQuery
      DefaultRowHeight = 20
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridMode = gmListBox
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 18
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      RowSelectMode = rsSingle
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      OnDblClick = AvailablePlaylistGridDblClick
      OnRowChanged = AvailablePlaylistGridRowChanged
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'Playlist_Description'
          Col.FieldName = 'Playlist_Description'
          Col.Heading = 'Playlist Description'
          Col.Width = 250
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'Entry_Operator'
          Col.FieldName = 'Entry_Operator'
          Col.Heading = 'Operator'
          Col.Width = 120
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'Entry_Date'
          Col.FieldName = 'Entry_Date'
          Col.Heading = 'Entry Date'
          Col.Width = 154
          Col.AssignedValues = '?'
        end>
    end
    object BitBtn1: TButton
      Left = 74
      Top = 224
      Width = 198
      Height = 33
      Caption = '&Load New Playlist'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
    end
    object BitBtn3: TButton
      Left = 320
      Top = 224
      Width = 198
      Height = 33
      Caption = 'Append to Current List'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 6
      ParentFont = False
      TabOrder = 2
    end
  end
end
