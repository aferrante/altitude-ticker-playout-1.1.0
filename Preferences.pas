unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, StBase, {StShBase, StBrowsr,} Spin, Mask,
  SUIImagePanel, SUIGroupBox, SUIRadioGroup, SUIEdit, SUIButton, SUIForm;

type
  TPrefs = class(TForm)
    Panel2: TPanel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Panel4: TPanel;
    Panel3: TPanel;
    Panel7: TPanel;
    StaticText3: TStaticText;
    Panel1: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    StationIDNum: TSpinEdit;
    BitBtn1: TButton;
    BitBtn2: TButton;
    Edit2: TEdit;
    Edit3: TEdit;
    RadioGroup4: TRadioGroup;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    RadioGroup6: TRadioGroup;
    Edit1: TEdit;
    RadioGroup5: TRadioGroup;
    StationIDDesc: TEdit;
    Label5: TLabel;
    LoggingIDNum: TSpinEdit;
    Label6: TLabel;
    Edit4: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Prefs: TPrefs;

implementation

{$R *.DFM}

end.
