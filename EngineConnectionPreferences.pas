unit EngineConnectionPreferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls, Spin, SUIEdit, SUIButton, SUIForm;

type
  TEnginePrefsDlg = class(TForm)
    Panel2: TPanel;
    Label4: TLabel;
    BitBtn1: TButton;
    BitBtn2: TButton;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    EngineIPAddress: TEdit;
    EnginePort: TEdit;
    RadioGroup1: TRadioGroup;
    EngineBaudRate: TComboBox;
    EngineComPortNumber: TComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EnginePrefsDlg: TEnginePrefsDlg;

implementation

{$R *.DFM}

end.
