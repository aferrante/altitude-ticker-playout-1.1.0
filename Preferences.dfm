object Prefs: TPrefs
  Left = 478
  Top = 131
  BorderStyle = bsDialog
  Caption = 'User Preferences'
  ClientHeight = 641
  ClientWidth = 624
  Color = clBtnFace
  Constraints.MinHeight = 29
  Constraints.MinWidth = 150
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TButton
    Left = 223
    Top = 594
    Width = 81
    Height = 38
    Caption = 'OK'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
  end
  object BitBtn2: TButton
    Left = 319
    Top = 594
    Width = 81
    Height = 38
    Cancel = True
    Caption = 'Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 16
    Top = 8
    Width = 593
    Height = 201
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 253
      Height = 16
      Caption = 'SQL Server ADO Connection Strings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 88
      Width = 149
      Height = 16
      Caption = 'Sportbase Database:'
      FocusControl = BitBtn1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 16
      Top = 32
      Width = 158
      Height = 16
      Caption = 'Main Ticker Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 144
      Width = 235
      Height = 16
      Caption = 'Logo Image Filenames Database:'
      FocusControl = BitBtn1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit2: TEdit
      Left = 16
      Top = 56
      Width = 553
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Edit3: TEdit
      Left = 16
      Top = 112
      Width = 553
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Edit4: TEdit
      Left = 16
      Top = 168
      Width = 553
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel3: TPanel
    Left = 15
    Top = 215
    Width = 594
    Height = 106
    TabOrder = 4
    object RadioGroup1: TRadioGroup
      Left = 11
      Top = 8
      Width = 106
      Height = 84
      Caption = 'GPI Enable'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 0
      TabStop = True
    end
    object RadioGroup2: TRadioGroup
      Left = 275
      Top = 8
      Width = 162
      Height = 84
      Caption = 'GPI COM Port'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        'COM1'
        'COM2'
        'COM3'
        'COM4')
      ParentFont = False
      TabOrder = 1
      TabStop = True
    end
    object RadioGroup3: TRadioGroup
      Left = 447
      Top = 8
      Width = 130
      Height = 84
      Caption = 'GPI Baud Rate'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        '9600'
        '19200')
      ParentFont = False
      TabOrder = 2
      TabStop = True
    end
    object RadioGroup6: TRadioGroup
      Left = 127
      Top = 8
      Width = 138
      Height = 84
      Caption = 'GPI Enable (Alt.)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 3
      TabStop = True
    end
  end
  object Panel7: TPanel
    Left = 14
    Top = 414
    Width = 595
    Height = 71
    TabOrder = 5
    object StaticText3: TStaticText
      Left = 15
      Top = 10
      Width = 299
      Height = 20
      Caption = 'Sponsor Logo As-Run Files Directory Path:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Edit1: TEdit
      Left = 15
      Top = 32
      Width = 562
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'Edit1'
    end
  end
  object Panel1: TPanel
    Left = 240
    Top = 325
    Width = 369
    Height = 84
    TabOrder = 6
    object RadioGroup5: TRadioGroup
      Left = 179
      Top = 8
      Width = 182
      Height = 71
      Caption = 'Full Sponsor Pages'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 0
      TabStop = True
      Visible = False
    end
  end
  object Panel5: TPanel
    Left = 16
    Top = 492
    Width = 593
    Height = 93
    TabOrder = 7
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 152
      Height = 16
      Caption = 'Station ID Information:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 304
      Top = 32
      Width = 136
      Height = 16
      Caption = 'Station Description:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 32
      Width = 145
      Height = 16
      Caption = 'Station ID (Playlists):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 176
      Top = 32
      Width = 80
      Height = 16
      Caption = 'Logging ID:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object StationIDNum: TSpinEdit
      Left = 58
      Top = 56
      Width = 49
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 20
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
    end
    object StationIDDesc: TEdit
      Left = 304
      Top = 56
      Width = 273
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 'StationIDDesc'
    end
    object LoggingIDNum: TSpinEdit
      Left = 194
      Top = 56
      Width = 49
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 20
      MinValue = 1
      ParentFont = False
      TabOrder = 2
      Value = 1
    end
  end
  object Panel4: TPanel
    Left = 201
    Top = 325
    Width = 221
    Height = 84
    TabOrder = 3
    object RadioGroup4: TRadioGroup
      Left = 19
      Top = 8
      Width = 182
      Height = 71
      Caption = 'Schedule Monitoring'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 0
      TabStop = True
    end
  end
end
