unit AsRunLogViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids_ts, TSGrid, TSDBGrid;

type
  TAsRunLogViewerDlg = class(TForm)
    AsRunLogGrid: TtsDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AsRunLogViewerDlg: TAsRunLogViewerDlg;

implementation

{$R *.DFM}

//Handler for export data button
procedure TAsRunLogViewerDlg.BitBtn2Click(Sender: TObject);
begin
  AsRunLogGrid.ExportGrid;
end;

end.
