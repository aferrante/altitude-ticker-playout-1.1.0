object MasterControlInterface: TMasterControlInterface
  Left = 904
  Top = 317
  Width = 186
  Height = 140
  Caption = 'MasterControlInterface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MasterControlPort: TServerSocket
    Active = False
    Port = 3395
    ServerType = stNonBlocking
    OnAccept = MasterControlPortAccept
    OnClientConnect = MasterControlPortClientConnect
    OnClientDisconnect = MasterControlPortClientDisconnect
    OnClientRead = MasterControlPortClientRead
    OnClientError = MasterControlPortClientError
    Left = 51
    Top = 32
  end
  object MCStatusUpdateTimer: TTimer
    Enabled = False
    OnTimer = MCStatusUpdateTimerTimer
    Left = 96
    Top = 32
  end
end
