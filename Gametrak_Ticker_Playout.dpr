program Gametrak_Ticker_Playout;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  Preferences in 'Preferences.pas' {Prefs},
  AboutBox in 'AboutBox.pas' {About},
  DataModule in 'DataModule.pas' {dmMain: TDataModule},
  SearchDataEntry in 'SearchDataEntry.pas' {TextSearchDlg},
  AsRunLogViewer in 'AsRunLogViewer.pas' {AsRunLogViewerDlg},
  NoteEntryEditor in 'NoteEntryEditor.pas' {NoteEntryEditorDlg},
  ScheduleEntryTimeEditor in 'ScheduleEntryTimeEditor.pas' {ScheduleEntryEditDlg},
  NCAAManualGameEntry in 'NCAAManualGameEntry.pas' {NCAAMatchupManualDlg},
  Globals in 'Globals.pas',
  ZipperEntry in 'ZipperEntry.pas' {ZipperEntryDlg},
  EngineIntf in 'EngineIntf.pas' {EngineInterface},
  EngineConnectionPreferences in 'EngineConnectionPreferences.pas' {EnginePrefsDlg},
  CheckTickerSchedule in 'CheckTickerSchedule.pas',
  PlaylistSelect in 'PlaylistSelect.pas' {PlaylistSelectDlg},
  GameDataFunctions in 'GameDataFunctions.pas',
  MasterControlInterfaceModule in 'MasterControlInterfaceModule.pas' {MasterControlInterface},
  OutgoingEMailModule in 'OutgoingEMailModule.pas' {OutgoingEMail},
  LoggingFunctionsModule in 'LoggingFunctionsModule.pas' {LoggingFunctions},
  CrispinInterfaceModule in 'CrispinInterfaceModule.pas' {CrispinInterface};

{$R *.RES}
{$H+}

begin
  Application.Initialize;
  Application.Title := 'Gametrak_Ticker_Playout';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TPrefs, Prefs);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TTextSearchDlg, TextSearchDlg);
  Application.CreateForm(TAsRunLogViewerDlg, AsRunLogViewerDlg);
  Application.CreateForm(TNoteEntryEditorDlg, NoteEntryEditorDlg);
  Application.CreateForm(TScheduleEntryEditDlg, ScheduleEntryEditDlg);
  Application.CreateForm(TNCAAMatchupManualDlg, NCAAMatchupManualDlg);
  Application.CreateForm(TZipperEntryDlg, ZipperEntryDlg);
  Application.CreateForm(TEngineInterface, EngineInterface);
  Application.CreateForm(TEnginePrefsDlg, EnginePrefsDlg);
  Application.CreateForm(TPlaylistSelectDlg, PlaylistSelectDlg);
  Application.CreateForm(TOutgoingEMail, OutgoingEMail);
  Application.CreateForm(TLoggingFunctions, LoggingFunctions);
  Application.CreateForm(TMasterControlInterface, MasterControlInterface);
  Application.CreateForm(TCrispinInterface, CrispinInterface);
  Application.Run;
end.
