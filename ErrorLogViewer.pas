unit ErrorLogViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids_ts, TSGrid, TSDBGrid;

type
  TErrorLogViewerDlg = class(TForm)
    ErrorLogGrid: TtsDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ErrorLogViewerDlg: TErrorLogViewerDlg;

implementation

{$R *.DFM}

//Handler for export data button
procedure TErrorLogViewerDlg.BitBtn2Click(Sender: TObject);
begin
  ErrorLogGrid.ExportGrid;
end;

end.
