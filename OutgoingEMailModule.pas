unit OutgoingEMailModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdMessage, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdMessageClient, IdSMTP, Globals;

type
  TOutgoingEMail = class(TForm)
    SMTP: TIdSMTP;
    MailMessage: TIdMessage;
  private
    { Private declarations }
  public
    procedure SendEMail (EMail_From_Address, EMail_To_Addresses, SubjectText, MessageBody: String);
  end;

var
  OutgoingEMail: TOutgoingEMail;

implementation

uses LoggingFunctionsModule, EngineIntf;

//Procedure to send the e-mail
procedure TOutgoingEMail.SendEMail (EMail_From_Address, EMail_To_Addresses, SubjectText, MessageBody: String);
begin
  if (EMailAlertsEnabled) then
  try
    //Setup the e-mail message
    MailMessage.From.Address := EMail_From_Address;
    MailMessage.Recipients.EMailAddresses := EMail_To_Addresses;

    MailMessage.Subject := SubjectText;
    MailMessage.Body.Text := MessageBody;

    //Send the e-mail
    try
      try
        SMTP.Connect(1000);
        SMTP.Send(MailMessage);
      except on E:Exception do
        begin
          //StatusMemo.Lines.Insert(0, 'ERROR: ' + E.Message);
          //Log error
          if (ErrorLoggingEnabled = True) then
          begin
            //WriteToErrorLog
            LoggingFunctions.WriteToSQLLog(StationIDForLogs, 500, E.Message);
            EngineInterface.WriteToErrorLog('0502: Error reported ny SMTP client while trying to send e-mail alert');
          end;
        end;
      end;
    finally
      if (SMTP.Connected) then SMTP.Disconnect;
    end;
  except
    //Log error
    if (ErrorLoggingEnabled = True) then
    begin
      //WriteToErrorLog
      LoggingFunctions.WriteToSQLLog(StationIDForLogs, 501, '');
      EngineInterface.WriteToErrorLog('0501: General error occurred while trying to send e-mail alert');
    end;
  end;
end;

{$R *.dfm}

end.
